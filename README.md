# TravellaLog
TravellaLog is a blogging software designed to write a travel blog.
Your travel blog that runs everywhere and independent of a commercial service.

![screenshot](https://bytebucket.org/MarkusKramer/travellalog/wiki/img/demo.png)

## Features
What you don't easily get with standard blogging solutions:

 * *Runs anywhere*. Your blog can be stored on a simple free webspace provider or run from your local hard drive (i.e. no application server or database needed)
 * You're *not dependent on a commercial service* to store your postings, pictures and videos which may shutdown at some point in the future. Your travel experiences will be available forever.
 * You could even *burn your blog on DVD*!
 * You can store pictures and movies in *perfect quality*.
 * *Google Earth* integration! You can store geo coordinates for each blog post. You can then display your journey on a map.
 * Possibility to write *private entries* which are not visible to anyone (not even the server admin).



## Quick start
Getting started with the TravellaLog software is easy. Just

 * download the latest release
 * open the _web/index.html_ in your web browser

To add or modify entries look into _web/public_journal/journal.xml_.
The _journal.xml_ is described in the documentation.

If you'd like to make modification to the look & feel of your blog look into the _web/user_ directory. You can change the design in _web/user/user.css_.
Keeping you changes in the _web/user_ directory will make it easier for you to update the blog software at a later point.



## How it works

 * Entries (blog posts) are stored in a simple XML file. Thus, your data is easily accessible and not hidden in some complex format (you could use XSLT to convert your travel blog into any format you like).
 * Implemented with the Google Web Toolkit (GWT). This means the blog runs completely with java script and no server components like databases, application server etc. are required. The blog runs on free web space providers, from your local hard disk, DVD etc.
 * Entries are organized in journals. For instance, one could have a public and a private journal. A journal is a directory containing the XML file with the journal's blog entries and associated pictures and movies. In order to protect a private journal you could use .htaccess on it's directory. The easiest and safest way to make sure that your private journal is not publicly accessible, is to not upload the directory to the web server at all.
