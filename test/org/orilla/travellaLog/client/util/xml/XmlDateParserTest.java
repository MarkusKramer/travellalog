package org.orilla.travellaLog.client.util.xml;

import java.util.Date;

import junit.framework.Assert;

import com.google.gwt.junit.client.GWTTestCase;


/**
 * @author Markus
 * 
 */
public class XmlDateParserTest extends GWTTestCase {

	@Override
	public String getModuleName() {
		return "org.orilla.travellaLog.TravellaLog";
	}

	public void testParseDate() {
		String str = "2008-12-31";
		Date date = XmlDateParser.parseDate(str);
		Assert.assertTrue(date.toString().startsWith("Wed Dec 31 00:00:00"));
	}

	public void testDateTime() {
		String str = "2010-01-01T13:14:15";
		Date date = XmlDateParser.parseDateTime(str);
		Assert.assertTrue(date.toString().startsWith("Fri Jan 01 13:14:15"));

		// format
		String str2 = XmlDateParser.formatDateTime(date);
		Assert.assertEquals(str, str2);
	}

}
