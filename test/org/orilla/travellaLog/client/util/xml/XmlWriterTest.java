package org.orilla.travellaLog.client.util.xml;

import junit.framework.Assert;

import org.junit.Test;


/**
 * @author Markus
 * 
 */
public class XmlWriterTest {

	@Test
	public void testXmlWriter() {
		XmlWriter xml = new XmlWriter();

		xml.beginElement("GeoData", "xmlns='http://geo.net/data'");
		xml.beginElement("Cities");

		xml.beginElement("Australia").print("Sydney").endElement();
		xml.beginElement("Germany").printLine("Berlin").printLine("Munich").endElement();

		xml.endElement();
		xml.endElement();

		String str = xml.toString();
		System.out.println(str);

		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "\n" + "<GeoData xmlns='http://geo.net/data'>\n" + "  <Cities>\n"
				+ "    <Australia>Sydney</Australia>\n" + "    <Germany>\n" + "      Berlin\n" + "      Munich\n" + "    </Germany>\n"
				+ "  </Cities>\n" + "</GeoData>";

		Assert.assertEquals(expected, str);
	}

}
