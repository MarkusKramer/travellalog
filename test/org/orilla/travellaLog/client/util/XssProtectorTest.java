package org.orilla.travellaLog.client.util;

import junit.framework.Assert;

import org.junit.Test;


/**
 * @author Markus
 * 
 */
public class XssProtectorTest {

	@Test
	public void encoding() {
		String str = "hello <danger /> ' \" & äüß";

		String fullEncodeResult = "hello&#32;&#60;danger&#32;&#47;&#62;&#32;&#39;&#32;&#34;&#32;&#38;&#32;&#228;&#252;&#223;";
		Assert.assertEquals(fullEncodeResult, XssProtector.fullEncode(str));

		String big5encodeResult = "hello &lt;danger /&gt; &#39; &quot; &amp; äüß";
		Assert.assertEquals(big5encodeResult, XssProtector.big5encode(str));
	}

}
