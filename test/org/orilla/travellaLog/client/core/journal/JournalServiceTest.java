package org.orilla.travellaLog.client.core.journal;

import java.util.List;

import org.orilla.travellaLog.client.core.journal.JournalService;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.domain.EventPicture;
import org.orilla.travellaLog.client.domain.EventVideo;
import org.orilla.travellaLog.client.domain.MediaItem;

import com.google.gwt.junit.client.GWTTestCase;


/**
 * @author Markus
 * 
 */
public class JournalServiceTest extends GWTTestCase {

	@Override
	public String getModuleName() {
		return "org.orilla.travellaLog.TravellaLog";
	}

	public void testSimple() {
		String xml = "<journal>\n";
		xml += "<entry><title>Title1</title><date>2010-02-13</date><content>C1</content></entry>\n";
		xml += "<entry><title>Title2</title><content>C2</content><date>2010-02-14</date></entry>\n";
		xml += "</journal>\n";

		List<Entry> entries = JournalService.INSTANCE.parseJournalXml(xml, "testJournal");
		assertEquals(2, entries.size());
		assertEquals("Title1", entries.get(0).title);
		assertEquals("C2", entries.get(1).events.get(0).content);
		assertNotNull(entries.get(0).date);
	}

	public void testMedia() {
		String xml = "<journal>\n";
		xml += "<entry><date>2010-02-13</date><content>C1</content>";
		xml += "<media><picture title=\"t1\">P1</picture><video title=\"t2\">V1</video>  </media>";
		xml += "</entry>\n";
		xml += "</journal>\n";

		List<Entry> entries = JournalService.INSTANCE.parseJournalXml(xml, "testJournal");
		List<MediaItem> items = entries.get(0).events.get(0).media;
		assertEquals(2, items.size());
		assertEquals("t1", ((EventPicture) items.get(0)).title);
		assertEquals("P1", ((EventPicture) items.get(0)).filename);
		assertEquals("t2", ((EventVideo) items.get(1)).title);
		assertEquals("V1", ((EventVideo) items.get(1)).filename);
	}

}
