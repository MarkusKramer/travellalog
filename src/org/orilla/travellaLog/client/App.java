package org.orilla.travellaLog.client;

import org.orilla.travellaLog.client.core.config.Config;
import org.orilla.travellaLog.client.core.config.JsConfig;


/**
 * @author Markus
 */
public class App {

	private static Config config = new JsConfig();

	public static Config getConfig() {
		return config;
	}

}
