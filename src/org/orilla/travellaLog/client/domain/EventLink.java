package org.orilla.travellaLog.client.domain;

/**
 * A hyper link that belongs to an {@link Event}.
 * @author Markus
 */
public class EventLink {

	public String title;
	public String url;

}
