package org.orilla.travellaLog.client.domain;

/**
 * A picture.
 * @author Markus
 */
public class EventPicture implements MediaItem {

	public String title;
	public String filename;

	/**
	 * @see org.orilla.travellaLog.client.domain.MediaItem#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

}
