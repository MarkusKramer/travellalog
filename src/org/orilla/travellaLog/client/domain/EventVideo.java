package org.orilla.travellaLog.client.domain;

/**
 * A locally stored video file.
 * @author Markus
 */
public class EventVideo implements MediaItem {

	public String title;
	public String filename;

	/**
	 * @see org.orilla.travellaLog.client.domain.MediaItem#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

}
