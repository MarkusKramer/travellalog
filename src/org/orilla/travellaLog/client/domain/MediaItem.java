package org.orilla.travellaLog.client.domain;

/**
 * A media item. Could be a picture, an AVI,MP4,... or a YouTube video.
 * @author Markus
 */
public interface MediaItem {

	/**
	 * Returns the title of the media item.
	 */
	String getTitle();

}
