package org.orilla.travellaLog.client.domain;

/**
 * A GPS geo coordinate in the same format as used by Google Maps.
 * @author Markus
 */
public class GeoCoordinate {

	public String ns;
	public String ew;

	public GeoCoordinate(String ns, String ew) {
		this.ns = ns;
		this.ew = ew;
	}

	public String toKmlCoordinate() {
		return ew + "," + ns;
	}

}
