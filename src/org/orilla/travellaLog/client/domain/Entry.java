package org.orilla.travellaLog.client.domain;

import java.util.Date;
import java.util.List;

import org.orilla.travellaLog.client.ext.googleearth.GoogleEarthService;


/**
 * A day's entry in a journal.
 * @author Markus
 */
public class Entry {

	public String title;
	public Date date;

	public List<Event> events;

	/**
	 * Indicates that on the end of the day one returned to one's former coordinate.
	 * Important to be able to draw correct paths in {@link GoogleEarthService} which has
	 * to keep track of a home coordinate.
	 */
	public boolean dayTrip = false;

	public String fromJournal;

}
