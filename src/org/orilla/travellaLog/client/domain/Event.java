package org.orilla.travellaLog.client.domain;

import java.util.List;


/**
 * An event in an {@link Entry}. An entry might have several events. Events store a
 * {@link GeoCoordinate}, thus it is possible to store the way points of a day trip.
 * 
 * @author Markus
 */
public class Event {

	public String title;
	public String content;
	public GeoCoordinate geoCoordinate;
	public List<MediaItem> media;
	public List<EventLink> links;

	public Entry parentEntry;

}
