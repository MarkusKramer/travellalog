package org.orilla.travellaLog.client.util;

import java.util.Date;


/**
 * @author Markus
 * 
 */
public class NoCache {

	public static String get() {
		return "?nocache=" + new Date().getTime();
	}

}
