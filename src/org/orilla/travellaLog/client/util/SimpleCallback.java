package org.orilla.travellaLog.client.util;

/**
 * A general purpose callback interface.
 * @author Markus
 */
public interface SimpleCallback<T> {

	void fire(T payload);

}
