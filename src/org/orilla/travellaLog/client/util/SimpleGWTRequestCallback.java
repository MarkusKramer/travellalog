package org.orilla.travellaLog.client.util;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;


/**
 * @author Markus
 * 
 */
public abstract class SimpleGWTRequestCallback implements RequestCallback {

	public void onError(Request request, Throwable exception) {
		throw new RuntimeException("Request failed. " + exception.getMessage());
	}

	public void onResponseReceived(Request request, Response response) {
		if (response.getStatusCode() != 200) {
			throw new RuntimeException("Request responded with status code " + response.getStatusCode() + ": " + response.getStatusText());
		}
		onResponseText(response.getText());
	}

	public abstract void onResponseText(String text);

}
