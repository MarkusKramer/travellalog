package org.orilla.travellaLog.client.util;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JsArrayString;


/**
 * @author Markus
 * 
 */
public class JsniUtil {

	public static native void setupJsLogging() /*-{
		$wnd.log = function(msg){
		@com.allen_sauer.gwt.log.client.Log::debug(Ljava/lang/String;)(msg);
		};
	}-*/;

	public static JsArrayString getJsArray(List<String> list) {
		JsArrayString jsArray = createArray(list.size());
		for (int i = 0; i < list.size(); i++) {
			jsArray.set(i, list.get(i));
		}
		return jsArray;
	}

	public static List<String> toStringList(JsArrayString jsArray) {
		List<String> list = new ArrayList<String>(jsArray.length());
		for (int i = 0; i < jsArray.length(); i++) {
			String val = jsArray.get(i);
			list.add(val);
		}
		return list;
	}

	private static native JsArrayString createArray(int size) /*-{
		return new Array(size);
	}-*/;

}
