package org.orilla.travellaLog.client.util;

import com.google.gwt.user.client.Window;


/**
 * @author Markus
 * 
 */
public class BrowserCheck {

	public void checkFirefox() {
		String agent = getUserAgent().toLowerCase();
		if (!agent.contains("firefox")) {
			Window.alert("At this moment only Firefox is supported!");
		}
	}

	public static native String getUserAgent() /*-{
		return navigator.userAgent.toLowerCase();
	}-*/;

}
