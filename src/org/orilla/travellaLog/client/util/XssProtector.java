package org.orilla.travellaLog.client.util;

/**
 * @author Markus
 * 
 */
public class XssProtector {

	public static String fullEncode(String s) {
		if (s == null) {
			return null;
		}

		StringBuilder buf = new StringBuilder(s.length());
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9') {
				buf.append(c);
			} else {
				buf.append("&#").append((int) c).append(";");
			}
		}
		return buf.toString();
	}

	public static String big5encode(String s) {
		if (s == null) {
			return null;
		}

		s = s.replaceAll("&", "&amp;");
		s = s.replaceAll("'", "&#39;");
		s = s.replaceAll("\"", "&quot;");
		s = s.replaceAll("<", "&lt;");
		s = s.replaceAll(">", "&gt;");
		return s;
	}

	public static String secureUrl(String url) {
		if (url == null) {
			return null;
		}

		if (url.startsWith("http://") || url.startsWith("https://") || url.startsWith("ftp://")) {
			return big5encode(url);
		} else {
			return fullEncode(url);
		}
	}
}
