package org.orilla.travellaLog.client.util;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * @author Markus
 * 
 */
public class ResourceLoader {

	public static void loadXml(final String url, final SimpleCallback<String> loadedCallback) {
		// Log.debug("Loading resource from " + url);

		if (!url.startsWith("file://")) {
			// using GWT's RequestBuilder
			RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);
			try {
				builder.sendRequest(null, new RequestCallback() {
					@Override
					public void onResponseReceived(Request request, Response response) {
						if (response.getStatusCode() == 200) {
							String content = response.getText();
							loadedCallback.fire(content);
						} else {
							Log.warn("Request for " + url + " failed. Status code " + response.getStatusCode() + ": " + response.getStatusText());
							loadedCallback.fire(null);
						}
					}

					@Override
					public void onError(Request request, Throwable exception) {
						throw new RuntimeException("Request failed. " + exception.getMessage());
					}
				});
			} catch (RequestException e) {
				throw new RuntimeException("Building request failed", e);
			}

		} else {
			// using IFrame hack - to bypass Firefox's tight security
			Frame iframe = new Frame(url) {
				@Override
				public void onBrowserEvent(Event event) {
					super.onBrowserEvent(event);
					if (DOM.eventGetType(event) == Event.ONLOAD) {
						String content = serializeElement(getElement());
						loadedCallback.fire(content);

						IFrameElement frameElement = IFrameElement.as(this.getElement());
						frameElement.removeFromParent();
					}
				}
			};
			iframe.sinkEvents(Event.ONLOAD);
			iframe.getElement().getStyle().setDisplay(Display.NONE);
			RootPanel.get().add(iframe);
		}
	}

	private static String serializeElement(Element element) {
		// TODO doesn't work on IE, maybe use NodeImpl.toString
		return _serializeElement(element);
	}

	public static native String _serializeElement(Element element) /*-{
		return (new XMLSerializer()).serializeToString(element.contentWindow.document);
	}-*/;

}
