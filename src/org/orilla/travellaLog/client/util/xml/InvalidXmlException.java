package org.orilla.travellaLog.client.util.xml;

import com.google.gwt.xml.client.Element;


/**
 * @author Markus
 * 
 */
public class InvalidXmlException extends RuntimeException {

	private Element element;

	public InvalidXmlException(String message, Element element) {
		super(message);
		this.element = element;
	}

	/**
	 * @see java.lang.Throwable#toString()
	 */
	@Override
	public String toString() {
		String elementHtml = element.toString();
		return super.toString() + ":\n" + elementHtml;
	}

}
