package org.orilla.travellaLog.client.util.xml;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;


/**
 * @author Markus
 * 
 */
public class XmlDateParser {

	private static DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
	private static DateTimeFormat timeFormat = DateTimeFormat.getFormat("HH:mm:ss");

	@SuppressWarnings("deprecation")
	public static Date parseDate(String str) {
		int day = Integer.valueOf(str.split("-")[2]);
		int month = Integer.valueOf(str.split("-")[1]) - 1;
		int year = Integer.valueOf(str.split("-")[0]) - 1900;
		Date date = new Date(year, month, day);
		return date;
	}

	@SuppressWarnings("deprecation")
	public static Date parseDateTime(String str) {
		String[] parts = str.split("T");
		String dateStr = parts[0];
		String timeStr = parts[1];
		Date date = parseDate(dateStr);

		String[] time = timeStr.split(":");
		date.setHours(Integer.valueOf(time[0]));
		date.setMinutes(Integer.valueOf(time[1]));
		date.setSeconds(Integer.valueOf(time[2]));
		return date;
	}

	public static String formatDateTime(Date d) {
		String date = dateFormat.format(d);
		String time = timeFormat.format(d);
		return date + "T" + time;
	}

}
