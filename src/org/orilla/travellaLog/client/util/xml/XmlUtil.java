package org.orilla.travellaLog.client.util.xml;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;


/**
 * @author Markus
 * 
 */
public class XmlUtil {

	public static String getValueNullsafe(Element element) {
		if( element.getFirstChild() != null ) {
			String value = element.getFirstChild().getNodeValue();
			return value;
		} else {
			throw new InvalidXmlException("Element '" + getNodeXPath(element) + "' has no value (no text node child)", element);
		}
	}
	
	public static String getValueOfChildNullsafe(Element element, String childTagName) {
		String value = getValueOfChild(element, childTagName);
		if (value == null) {
			throw new InvalidXmlException("Element '" + getNodeXPath(element) + "' must have a child element '" + childTagName + "'", element);
		}
		return value;
	}

	public static String getValueOfChild(Element element, String childTagName) {
		Element child = getChild(element, childTagName);
		if (child == null) {
			return null;
		} else {
			String value = "";
			NodeList nodes = child.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				String s = node.toString();
				// gwt bug workaround. &semi; is not valid XML
				// http://code.google.com/p/google-web-toolkit/issues/detail?id=1011
				s = s.replaceAll("&semi;", ";");
				value += s;
			}
			return value;
		}
	}

	/**
	 * Returns the child of the given element which has the given tag name, or null if
	 * none exists. Only direct children will be considered. Throws a exception if more
	 * than one is found.
	 */
	public static Element getChild(Element element, String childTagName) {
		List<Element> children = getElementByTagName(element, childTagName);
		if (children.isEmpty()) {
			return null;
		}
		if (children.size() > 1) {
			throw new InvalidXmlException("Element " + getNodeXPath(element) + " has more than one '" + childTagName + "' child", element);
		}

		return children.get(0);
	}

	/**
	 * Transforms all child elements of the given element into beans using the given
	 * {@link ElementParser}. Returns the resulting list of beans. Only direct children
	 * with the given tag name will be transformed.
	 */
	public static <T> List<T> parseChildren(Element element, String tagName, ElementParser<T> elementParser) {
		List<T> objs = new ArrayList<T>();
		for (Element child : getElementByTagName(element, tagName)) {
			T obj = elementParser.parse(child);
			objs.add(obj);
		}
		return objs;
	}

	/**
	 * A getElementsByTagName wrapper which returns an {@link ArrayList}.
	 */
	public static List<Element> getElementByTagName(Element element, String tagName) {
		List<Element> elements = new ArrayList<Element>();
		NodeList childElements = element.getElementsByTagName(tagName);
		for (int i = 0; i < childElements.getLength(); i++) {
			Element child = (Element) childElements.item(i);
			if (child.getParentNode().equals(element)) {
				elements.add(child);
			}
		}
		return elements;
	}

	/**
	 * Returns the XPath of the given {@link Node}.
	 */
	public static String getNodeXPath(Node node) {
		String path = "/" + node.getNodeName();
		Node parent = node.getParentNode();
		if (parent instanceof Document) {
			return path;
		}

		// add parent path
		path = getNodeXPath(parent) + path;

		// add index
		NodeList parentChildren = ((Element) parent).getElementsByTagName(node.getNodeName());
		for (int i = 0; i < parentChildren.getLength(); i++) {
			Node child = parentChildren.item(i);
			if (child.equals(node)) {
				path += "[" + i + "]";
				break;
			}
		}

		return path;
	}

}
