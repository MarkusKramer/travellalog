package org.orilla.travellaLog.client.util.xml;

import com.google.gwt.xml.client.Element;


/**
 * @author Markus
 * 
 */
public interface ElementParser<T> {

	public T parse(Element element);

}
