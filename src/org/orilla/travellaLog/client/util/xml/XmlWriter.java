package org.orilla.travellaLog.client.util.xml;

import java.util.Stack;


/**
 * Produces XML with nice indentation. Very simple and fast API to write XML. The methods
 * are designed to allow chaining of calls.
 * @author Markus
 */
public class XmlWriter {

	private int indentation = 2;

	private int currentIndent = 0;
	private StringBuilder sb = new StringBuilder();
	private Stack<String> elementNameStack = new Stack<String>();
	private boolean appendInSameLine = false;

	public XmlWriter() {
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	}

	public XmlWriter beginElement(String elementName) {
		return beginElement(elementName, null);
	}

	/**
	 * Begins a new XML element in a new line.
	 * @param elementName name of the element
	 * @param attributes contains all the attributes for this element
	 */
	public XmlWriter beginElement(String elementName, String attributes) {
		elementNameStack.push(elementName);

		indent();

		sb.append('<');
		sb.append(elementName);
		if (attributes != null) {
			sb.append(' ');
			sb.append(attributes);
		}
		sb.append('>');

		currentIndent += indentation;
		return this;
	}

	/**
	 * Insert an end tag for the current XML element opened with
	 * {@link #beginElement(String, String)}. Insert it in a new line, expect when only
	 * text has been added with {@link #print(String)}.
	 */
	public XmlWriter endElement() {
		currentIndent -= indentation;
		String elementName = elementNameStack.pop();

		if (!appendInSameLine) {
			indent();
		} else {
			appendInSameLine = false;
		}

		sb.append('<');
		sb.append('/');
		sb.append(elementName);
		sb.append('>');

		return this;
	}

	/**
	 * Appends text at the current position (in the same line as the last
	 * {@link #beginElement(String)} call).
	 */
	public XmlWriter print(String text) {
		sb.append(text);
		appendInSameLine = true;
		return this;
	}

	/**
	 * Prints a complete line of text. Inserts it in a new line.
	 */
	public XmlWriter printLine(String text) {
		indent();
		sb.append(text);
		return this;
	}

	private void indent() {
		sb.append('\n');
		for (int i = 0; i < currentIndent; i++) {
			sb.append(' ');
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (!elementNameStack.isEmpty()) {
			throw new IllegalStateException("XML not finished. Element '" + elementNameStack.peek() + "' is still open.");
		}
		return sb.toString();
	}

	/**
	 * Sets the indentation to be used.
	 */
	public void setIndentation(int indentation) {
		this.indentation = indentation;
	}

}
