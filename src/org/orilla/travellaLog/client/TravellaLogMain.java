package org.orilla.travellaLog.client;

import java.util.Date;
import java.util.List;

import org.orilla.travellaLog.client.core.LoadingWidget;
import org.orilla.travellaLog.client.core.TitleBannerWidget;
import org.orilla.travellaLog.client.core.entryfilter.EntriesFilterWidget;
import org.orilla.travellaLog.client.core.entryfilter.MonthSelectionWidget;
import org.orilla.travellaLog.client.core.errordisplay.ErrorDisplayWidget;
import org.orilla.travellaLog.client.core.journal.DateStatistics;
import org.orilla.travellaLog.client.core.journal.EntryDateOrdering;
import org.orilla.travellaLog.client.core.journal.JournalService;
import org.orilla.travellaLog.client.core.journal.widget.EntriesListViewWidget;
import org.orilla.travellaLog.client.core.journal.widget.JournalEntryWidget;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.ext.comment.CommentBoxWidget;
import org.orilla.travellaLog.client.ext.googleearth.GoogleEarthSidebarWidget;
import org.orilla.travellaLog.client.ext.mailinglist.MailinglistWidget;
import org.orilla.travellaLog.client.util.JsniUtil;
import org.orilla.travellaLog.client.util.SimpleCallback;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;
import com.google.gwt.dom.client.Document;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;


public class TravellaLogMain implements EntryPoint {

	EntriesFilterWidget efw;
	
	public static final DateTimeFormat urlDateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		// setup exception handling
		GWT.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			public void onUncaughtException(Throwable e) {
				handleUncaughtError(e);
			}
		});

		// setup native JS logging
		JsniUtil.setupJsLogging();

		// show loading info
		Document.get().getElementById("contentContainer").setInnerHTML("");
		setContentContainer(new LoadingWidget());

		// load journals
		JournalService.INSTANCE.init(new SimpleCallback<Void>() {
			public void fire(Void payload) {
				showApplication();
			}
		});
	}

	private void showApplication() {
		// title banner
		TitleBannerWidget titleBannerWidget = new TitleBannerWidget();
		RootPanel.get("headerContainer").add(titleBannerWidget);

		// sidebar
		fillSidebar();

		// filter change listener
		SimpleCallback<Object> onFilterChangeCallback = new SimpleCallback<Object>() {
			public void fire(Object payload) {
				setContentContainer(new LoadingWidget());
				DeferredCommand.addCommand(new Command() {
					public void execute() {
						performQuery();
					}
				});
			}
		};
		efw.setOnChangeCallback(onFilterChangeCallback);

		// initially show entries
		onFilterChangeCallback.fire(null);
	}

	private void fillSidebar() {
		// AdminSidebarWidget
		// if (RootPanel.get("adminSidebarWidget") != null && !GWT.isScript()) {
		// AdminSidebarWidget adminSidebarWidget = new AdminSidebarWidget();
		// Widget widget = wrapIntoDisclosurePanel("Administration", adminSidebarWidget,
		// false);
		// RootPanel.get("adminSidebarWidget").add(widget);
		// }

		// MonthSelectionWidget
		if (RootPanel.get("monthSelectionWidget") != null) {
			DateStatistics dateStatistics = JournalService.INSTANCE.getDateStatistics();
			efw = new MonthSelectionWidget(dateStatistics);
			Widget widget = wrapIntoDisclosurePanel("Auswahl", efw, true);
			RootPanel.get("monthSelectionWidget").add(widget);
		}

		// GoogleEarthSidebarWidget
		if (RootPanel.get("googleEarthSidebarWidget") != null) {
			Widget widget = wrapIntoDisclosurePanel("Google Earth", new GoogleEarthSidebarWidget(), true);
			RootPanel.get("googleEarthSidebarWidget").add(widget);
		}

		// MailinglistWidget
		if (RootPanel.get("mailinglistWidget") != null) {
			Widget widget = wrapIntoDisclosurePanel("Mailingliste", new MailinglistWidget(), false);
			RootPanel.get("mailinglistWidget").add(widget);
		}

		// CommentBoxWidget
		if (RootPanel.get("commentBoxWidget") != null) {
			Widget widget = wrapIntoDisclosurePanel("Kommentare", new CommentBoxWidget(), true);
			RootPanel.get("commentBoxWidget").add(widget);
		}
	}

	private void performQuery() {
		// adjust filter to include specified date
		String hash = Window.Location.getHash();
		Date showDate = null;
		if( !hash.isEmpty() ) {
			hash = hash.substring(1);
			showDate = urlDateFormat.parse(hash);
			Log.info("Including date "+showDate.toString()+" in filter selection");
			efw.addDateToSelection(showDate);
		}
		
		List<Entry> entries = JournalService.INSTANCE.getEntries(efw.getSelectedJournalNames(), efw.getEntryFilter(), new EntryDateOrdering());
		Log.debug("Displaying results");
		EntriesListViewWidget entriesListViewWidget = new EntriesListViewWidget(entries);
		setContentContainer(entriesListViewWidget);
		
		if( !hash.isEmpty() ) {
			for(JournalEntryWidget entryWidget : entriesListViewWidget.getEntryWidgets() ) {
				if( entryWidget.getEntry().date.equals(showDate) ) {
					entryWidget.getElement().scrollIntoView();
					Window.scrollTo(Window.getScrollLeft(), Window.getScrollTop() + 200);
				}
			}
		}
		
		Log.debug("Query finished");
	}
	
	private DisclosurePanel wrapIntoDisclosurePanel(String title, Widget widget, boolean open) {
		DisclosurePanel disclosurePanel = new DisclosurePanel(title);
		disclosurePanel.add(widget);
		disclosurePanel.setOpen(open);
		return disclosurePanel;
	}

	public void handleUncaughtError(Throwable exception) {
		Log.error("Uncaught exception:" + exception.getMessage());
		exception.printStackTrace();
		setContentContainer(new ErrorDisplayWidget(exception));
	}

	private void setContentContainer(Widget widget) {
		RootPanel.get("contentContainer").clear();
		RootPanel.get("contentContainer").add(widget);
	}

}
