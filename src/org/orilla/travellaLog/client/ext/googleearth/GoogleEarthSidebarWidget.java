package org.orilla.travellaLog.client.ext.googleearth;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.orilla.travellaLog.client.core.journal.EntryDateOrdering;
import org.orilla.travellaLog.client.core.journal.EntryFilter;
import org.orilla.travellaLog.client.core.journal.JournalService;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.util.widgets.UlListPanel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;


/**
 * @author Markus
 * 
 */
public class GoogleEarthSidebarWidget extends FlowPanel {

	private List<Tour> tours = new ArrayList<Tour>();

	public GoogleEarthSidebarWidget() {
		addStyleName("googleEarthSidebarWidget");

		UlListPanel optionsPanel = new UlListPanel();
		add(optionsPanel);
		optionsPanel.addStyleName("optionsPanel optionsList");

		loadTours();

		for (final Tour tour : tours) {
			Anchor tourLink = new Anchor(tour.name);
			optionsPanel.add(tourLink);
			tourLink.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					GoogleEarthService.INSTANCE.showTour(tour);
				}
			});
		}
	}

	private void loadTours() {
		Tour worldTour = buildTour(null, null);
		worldTour.name = "World Tour";
		tours.add(worldTour);

		// TODO read more from config
	}

	private Tour buildTour(final Date begin, final Date end) {
		EntryFilter entryFilter = new EntryFilter() {
			@Override
			public boolean isValid(Entry entry) {
				if (begin != null && entry.date.compareTo(begin) < 0) {
					return false;
				}
				if (end != null && entry.date.compareTo(end) > 0) {
					return false;
				}
				return true;
			}
		};

		Tour tour = new Tour();
		tour.entries = JournalService.INSTANCE.getEntries(JournalService.INSTANCE.getAvailableJournalNames(), entryFilter, new EntryDateOrdering());
		return tour;
	}

}
