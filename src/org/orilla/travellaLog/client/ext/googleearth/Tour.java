package org.orilla.travellaLog.client.ext.googleearth;

import java.util.List;

import org.orilla.travellaLog.client.domain.Entry;


/**
 * A tour is essentially a list of certain {@link Entry}s.
 * @author Markus
 */
public class Tour {

	public String name;
	public List<Entry> entries;

}
