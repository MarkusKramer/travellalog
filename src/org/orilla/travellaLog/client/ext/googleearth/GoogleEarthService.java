package org.orilla.travellaLog.client.ext.googleearth;

import org.orilla.travellaLog.client.App;
import org.orilla.travellaLog.client.core.journal.MediaItemLocator;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.domain.Event;
import org.orilla.travellaLog.client.domain.GeoCoordinate;
import org.orilla.travellaLog.client.domain.MediaItem;
import org.orilla.travellaLog.client.util.xml.XmlWriter;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Navigator;


/**
 * Produces tour files in the KML file format which can be viewed in Google Earth.
 * @author Markus
 */
public enum GoogleEarthService {

	INSTANCE;

	public void showTour(Tour tour) {
		String kmlXml = buildKmlDocument(tour);
		// System.out.println(kmlXml);

		String userAgent = Navigator.getUserAgent().toLowerCase();
		System.out.println("userAgent: "+userAgent);
		boolean isLinux = userAgent.contains("linux");
		if( !isLinux ) {
			String googleMapsApiKey;
			if (GWT.isScript()) {
				googleMapsApiKey = App.getConfig().getGoogleMapsApiKeyLive();
			} else {
				// emulated environment and probably localhost
				googleMapsApiKey = App.getConfig().getGoogleMapsApiKeyDev();
			}
			showKmlDocument(tour.name, kmlXml, googleMapsApiKey);
		} else {
			offerKmlDownload(kmlXml);
//			String query = "?data=" + URL.encodeQueryString(kmlXml);
//			
//			
		}
	}
	
	private void offerKmlDownload(String kmlXml) {
		// TODO parameterize
		// GWT.getHostPageBaseURL() + "app/google_earth/kmlEcho.php";
		final String url = "http://tomatenmark.lima-city.de/oz2blog/app/google_earth/kmlEcho.php";
		
	    RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, url);
	    try {
	        builder.sendRequest(kmlXml, new RequestCallback() {
				@Override
				public void onResponseReceived(Request request, Response response) {
					Log.debug("Response was: "+response.getText());
					Log.debug("header: "+ response.getHeadersAsString());
					String sessionId = response.getText();
					Window.open(url + "?sessionId=" + sessionId, "_blank", "");
				}
				
				@Override
				public void onError(Request request, Throwable exception) {
					throw new RuntimeException("Request failed. " + exception.getMessage());
				}
			});
		} catch (RequestException e) {
			throw new RuntimeException("Building request failed", e);
		}
	}

	public native void showKmlDocument(String title, String kmlXml, String googleMapsApiKey) /*-{
		var showEarth = function(){
		geWindow.document.title = title;
		geWindow.loadGoogleEarth("earthDiv",googleMapsApiKey,function(){
		geWindow.showKml(kmlXml);
		});
		};

		var geWindow = $wnd.open("app/google_earth/earthView.html", "_blank", "");
		geWindow.onload = showEarth;
	}-*/;

	public String buildKmlDocument(Tour tour) {
		XmlWriter xml = new XmlWriter();

		xml.beginElement("kml", "xmlns='http://www.opengis.net/kml/2.2'");
		xml.beginElement("Document");

		// voyage name
		xml.beginElement("name").print(tour.name).endElement();

		// placemarks
		{
			for (Entry entry : tour.entries) {
				int i = 1;
				for (Event event : entry.events) {
					if (event.geoCoordinate != null) {
						if (event.title != null || event.content != null) {
							xml.beginElement("Placemark");

							String title = event.title;
							if (title == null) {
								title = entry.title + " Stopp " + i;
							}
							xml.beginElement("name").print(title).endElement();

							// description & thumbnails
							xml.beginElement("description").print(builtDescription(event)).endElement();

							// geoCoordinate
							xml.beginElement("Point");
							xml.beginElement("coordinates").print(event.geoCoordinate.toKmlCoordinate()).endElement();
							xml.endElement();

							xml.endElement();

							i++;
						}
					}
				}
			}
		}

		// path
		buildPath(tour, xml);

		xml.endElement();
		xml.endElement();

		return xml.toString();
	}

	/**
	 * Connects all the dots with a visible path. If an {@link Entry} has only one
	 * {@link Event} a path will be drawn to its coordinate from the a home coordinate.
	 * The home coordinate is always updates with the last location of a day tour.
	 */
	private void buildPath(Tour tour, XmlWriter xml) {
		// path style
		xml.beginElement("Style", "id='tourLineStyle'");
		xml.beginElement("LineStyle");
		xml.beginElement("color").print("7f3366CC").endElement(); // 7c2163de
		xml.beginElement("width").print("12").endElement();
		xml.endElement();
		xml.endElement();

		GeoCoordinate homeCoordinate = null;

		xml.beginElement("Placemark");
		{
			xml.beginElement("styleUrl").print("#tourLineStyle").endElement();

			xml.beginElement("MultiGeometry");
			xml.beginElement("LineString");
			{
				xml.beginElement("extrude").print("1").endElement();
				xml.beginElement("tessellate").print("1").endElement();

				xml.beginElement("coordinates");
				for (Entry entry : tour.entries) {
					if (entry.dayTrip && homeCoordinate != null) {
						xml.printLine(homeCoordinate.toKmlCoordinate());
					}

					for (Event event : entry.events) {
						if (event.geoCoordinate != null) {
							// update the home coordinate, unless it's a day trip
							if (!entry.dayTrip) {
								homeCoordinate = event.geoCoordinate;
							}
							xml.printLine(event.geoCoordinate.toKmlCoordinate());
						}
					}

					if (entry.dayTrip && homeCoordinate != null) {
						xml.printLine(homeCoordinate.toKmlCoordinate());
					}
				}
				xml.endElement();
			}
			xml.endElement().endElement();
		}
		xml.endElement();
	}

	private String builtDescription(Event event) {
		String media = "\n\n<div>\n";
		for (final MediaItem mediaItem : event.media) {
			media += "<img src='../../" + MediaItemLocator.INSTANCE.getThumbnailUrl(mediaItem, event) + "'/>\n";
		}
		media += "</div>\n";
		String description = "<![CDATA[<div style='width:200px;'>" + event.content + media + "</div>]]>";
		return description;
	}

}
