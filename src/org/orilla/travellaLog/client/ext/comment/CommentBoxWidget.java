package org.orilla.travellaLog.client.ext.comment;

import java.util.List;

import org.orilla.travellaLog.client.ext.comment.add.CommentAddWidget;
import org.orilla.travellaLog.client.ext.comment.show.CommentShowWidget;
import org.orilla.travellaLog.client.util.SimpleCallback;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * @author Markus
 * 
 */
public class CommentBoxWidget extends Composite {

	Panel mainPanel;
	Panel commentsPanel;

	public CommentBoxWidget() {
		mainPanel = new VerticalPanel();
		mainPanel.addStyleName("commentBox");
		initWidget(mainPanel);

		Button addCommentButton = new Button("Kommentar schreiben");
		mainPanel.add(addCommentButton);
		addCommentButton.addStyleName("addComment");
		addCommentButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				CommentAddWidget addWidget = CommentAddWidget.showPopup();
				addWidget.onAddCallback = new SimpleCallback<Comment>() {
					public void fire(Comment payload) {
						show();
					}
				};
			}
		});

		commentsPanel = new VerticalPanel();
		mainPanel.add(commentsPanel);
		show();
	}

	public void show() {
		commentsPanel.clear();

		CommentService.INSTANCE.getComments(new SimpleCallback<List<Comment>>() {
			public void fire(List<Comment> comments) {
				render(comments);
			}
		});
	}

	private void render(List<Comment> comments) {
		for (Comment comment : comments) {
			CommentShowWidget cw = new CommentShowWidget(comment);
			commentsPanel.add(cw);
		}
	}

}
