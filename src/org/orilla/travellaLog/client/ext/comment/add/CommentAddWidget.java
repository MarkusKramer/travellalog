package org.orilla.travellaLog.client.ext.comment.add;

import java.util.Date;

import org.orilla.travellaLog.client.ext.comment.Comment;
import org.orilla.travellaLog.client.ext.comment.CommentService;
import org.orilla.travellaLog.client.util.SimpleCallback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;


/**
 * @author Markus
 * 
 */
public class CommentAddWidget extends Composite {
	interface CommentAddUiBinder extends UiBinder<Widget, CommentAddWidget> {
		// empty
	}

	private static CommentAddUiBinder uiBinder = GWT.create(CommentAddUiBinder.class);

	private static DialogBox dialogBox;

	@UiField
	TextBox author;
	@UiField
	TextBox pictureUrl;
	@UiField
	TextArea content;
	@UiField
	Button submit;
	@UiField
	Button cancel;

	public SimpleCallback<Comment> onAddCallback;

	public CommentAddWidget() {
		initWidget(uiBinder.createAndBindUi(this));

		content.addStyleName("contentTextArea");
		submit.setText("Speichern");
		submit.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				submit();
			}
		});

		cancel.setText("Abbrechen");
		cancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});
	}

	public static CommentAddWidget showPopup() {
		dialogBox = new DialogBox(false, false);
		dialogBox.setGlassEnabled(true);
		dialogBox.setText("Kommentar schreiben");
		CommentAddWidget commentAddWidget = new CommentAddWidget();
		dialogBox.setWidget(commentAddWidget);
		dialogBox.setPopupPosition(450, 100);
		dialogBox.show();
		return commentAddWidget;
	}

	public void submit() {
		submit.setEnabled(false);

		final Comment comment = new Comment();
		comment.author = author.getText();
		comment.date = new Date();
		comment.pictureUrl = pictureUrl.getText();
		comment.content = content.getText();

		CommentService.INSTANCE.addComment(comment, new SimpleCallback<Boolean>() {
			public void fire(Boolean payload) {
				if (payload == true) {
					dialogBox.hide();
					onAddCallback.fire(comment);
				} else {
					Window.alert("Failed to add comment");
					submit.setEnabled(true);
				}
			}
		});
	}

}
