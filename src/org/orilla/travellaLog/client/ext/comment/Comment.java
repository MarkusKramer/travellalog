package org.orilla.travellaLog.client.ext.comment;

import java.util.Date;


/**
 * @author Markus
 * 
 */
public class Comment {

	public String author;
	public Date date;
	public String pictureUrl;
	public String content;

}
