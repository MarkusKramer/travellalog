package org.orilla.travellaLog.client.ext.comment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.orilla.travellaLog.client.util.NoCache;
import org.orilla.travellaLog.client.util.ResourceLoader;
import org.orilla.travellaLog.client.util.SimpleCallback;
import org.orilla.travellaLog.client.util.SimpleGWTRequestCallback;
import org.orilla.travellaLog.client.util.XssProtector;
import org.orilla.travellaLog.client.util.xml.XmlDateParser;
import org.orilla.travellaLog.client.util.xml.XmlUtil;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.URL;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;


/**
 * @author Markus
 * 
 */
public enum CommentService {

	INSTANCE;

	String commentsXmlUrl = URL.encode(GWT.getHostPageBaseURL() + "app/comment/comments.xml");
	String addCommentUrl = URL.encode(GWT.getHostPageBaseURL() + "app/comment/addComment.php");

	public void getComments(final SimpleCallback<List<Comment>> loadedCallback) {
		String url = commentsXmlUrl + NoCache.get();
		Log.info("Loading comments from " + url);

		ResourceLoader.loadXml(url, new SimpleCallback<String>() {
			@Override
			public void fire(String text) {
				List<Comment> comments = parseComments(text);
				Log.info(comments.size() + " comments loaded successfully");
				loadedCallback.fire(comments);
			}
		});
	}

	private List<Comment> parseComments(String xmlString) {
		List<Comment> comments = new ArrayList<Comment>();
		Document doc = XMLParser.parse(xmlString);
		NodeList commentNodes = doc.getElementsByTagName("comment");

		for (int i = 0; i < commentNodes.getLength(); i++) {
			Element cn = (Element) commentNodes.item(i);
			Comment comment = new Comment();
			comment.author = XmlUtil.getValueOfChild(cn, "author");
			comment.pictureUrl = XmlUtil.getValueOfChild(cn, "pictureUrl");
			comment.content = XmlUtil.getValueOfChild(cn, "content");
			String dateString = XmlUtil.getValueOfChild(cn, "date");
			comment.date = XmlDateParser.parseDateTime(dateString);

			comments.add(comment);
		}
		Collections.reverse(comments);
		return comments;
	}

	public void addComment(Comment comment, final SimpleCallback<Boolean> successCallback) {

		String author = XssProtector.fullEncode(comment.author);
		String pictureUrl = XssProtector.secureUrl(comment.pictureUrl);
		String content = XssProtector.fullEncode(comment.content);
		String date = XmlDateParser.formatDateTime(comment.date);

		String indent = "    ";
		String xml = "<comment>\n";
		xml += indent + "<author>" + author + "</author>\n";
		xml += indent + "<pictureUrl>" + pictureUrl + "</pictureUrl>\n";
		xml += indent + "<date>" + date + "</date>\n";
		xml += indent + "<content>" + content + "</content>\n";
		xml += "</comment>\n";

		try {
			String query = "?xml=" + URL.encodeComponent(xml);
			RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, addCommentUrl + query);
			builder.sendRequest(null, new SimpleGWTRequestCallback() {
				public void onResponseText(String text) {
					boolean success = text.contains("SUCCESS");
					if (!success) {
						Log.error("Service responded with: " + text);
					}
					successCallback.fire(success);
				}
			});
		} catch (RequestException e) {
			throw new RuntimeException("Failed to add comment", e);
		}
	}
}
