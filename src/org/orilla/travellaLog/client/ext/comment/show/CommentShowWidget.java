package org.orilla.travellaLog.client.ext.comment.show;

import org.orilla.travellaLog.client.ext.comment.Comment;
import org.orilla.travellaLog.client.util.XssProtector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;


/**
 * @author Markus
 * 
 */
public class CommentShowWidget extends Composite {

	interface CommentShowWidgetUiBinder extends UiBinder<Widget, CommentShowWidget> {
		// empty
	}

	private static CommentShowWidgetUiBinder uiBinder = GWT.create(CommentShowWidgetUiBinder.class);

	private DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd.MM.yyyy HH:mm");

	@UiField
	SpanElement author;
	@UiField
	SpanElement date;
	@UiField
	SpanElement content;
	@UiField
	Image picture;

	public CommentShowWidget(Comment comment) {
		initWidget(uiBinder.createAndBindUi(this));
		picture.addStyleName("picture");
		show(comment);
	}

	private void show(Comment comment) {
		author.setInnerHTML(comment.author);
		content.setInnerHTML(comment.content);
		date.setInnerHTML(dateFormat.format(comment.date));

		if (comment.pictureUrl != null && !comment.pictureUrl.trim().equals("")) {
			picture.setUrl(XssProtector.secureUrl(comment.pictureUrl));
		} else {
			picture.removeFromParent();
		}
	}
}
