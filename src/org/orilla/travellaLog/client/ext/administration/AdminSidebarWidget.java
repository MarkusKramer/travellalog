package org.orilla.travellaLog.client.ext.administration;

import org.orilla.travellaLog.client.ext.administration.MediaIntegrityService.MediaCheckResult;
import org.orilla.travellaLog.client.util.SimpleCallback;
import org.orilla.travellaLog.client.util.widgets.UlListPanel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;


/**
 * @author Markus
 * 
 */
public class AdminSidebarWidget extends FlowPanel {

	public AdminSidebarWidget() {
		addStyleName("adminSidebarWidget");

		UlListPanel optionsPanel = new UlListPanel();
		add(optionsPanel);
		optionsPanel.addStyleName("optionsPanel optionsList");

		Anchor mediaIntegrity = new Anchor("Media Integrity Check");
		optionsPanel.add(mediaIntegrity);
		mediaIntegrity.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				checkMediaAvailability();
			}
		});

		Anchor resizePictures = new Anchor("Create thumbnails");
		optionsPanel.add(resizePictures);
		resizePictures.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub

			}
		});
	}

	private void checkMediaAvailability() {

		// please wait
		final DialogBox dialogBox = new DialogBox(false, true);
		dialogBox.setGlassEnabled(true);
		dialogBox.setText("Media Integrity Check");
		dialogBox.setWidget(new Label("Please wait..."));
		dialogBox.show();
		dialogBox.center();

		// check
		MediaIntegrityService.INSTANCE.checkMediaAvailability(new SimpleCallback<MediaCheckResult>() {
			@Override
			public void fire(MediaCheckResult result) {
				String msg = "";
				if (result.hasErrors()) {
					for (String item : result.itemsNotInFilesystem) {
						msg += "Media item " + item + " declared in journal, but not found in file system\n";
					}
					for (String item : result.itemsNotInJournal) {
						msg += "Media item " + item + " found in file system but not used in the journals\n";
					}
				} else {
					msg = "Everything is okay";
				}

				// display
				Panel p = new HTMLPanel("<pre>" + msg + "</pre>");
				dialogBox.setWidget(p);
				dialogBox.center();
				dialogBox.setAutoHideEnabled(true);
			}
		});
	}
}
