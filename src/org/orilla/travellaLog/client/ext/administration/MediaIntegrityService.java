package org.orilla.travellaLog.client.ext.administration;

import java.util.ArrayList;
import java.util.List;

import org.orilla.travellaLog.client.App;
import org.orilla.travellaLog.client.core.journal.JournalService;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.domain.Event;
import org.orilla.travellaLog.client.domain.EventPicture;
import org.orilla.travellaLog.client.domain.EventVideo;
import org.orilla.travellaLog.client.domain.MediaItem;
import org.orilla.travellaLog.client.util.SimpleCallback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * @author Markus
 * 
 */
public enum MediaIntegrityService {

	INSTANCE;

	private JournalAdminServiceAsync journalAdminService = (JournalAdminServiceAsync) GWT.create(JournalAdminService.class);

	/**
	 * Checks that all media items referenced in the journals actually exist in the file
	 * system vice versa.
	 */
	public void checkMediaAvailability(final SimpleCallback<MediaCheckResult> callback) {
		journalAdminService.getAllMediaFiles(App.getConfig().getJournalNames(), new AsyncCallback<List<String>>() {
			@Override
			public void onSuccess(final List<String> mediaFileNames) {
				MediaCheckResult result = _checkMediaAvailability(mediaFileNames);
				callback.fire(result);
			}

			@Override
			public void onFailure(Throwable caught) {
				throw new RuntimeException(caught);
			}
		});
	}

	private MediaCheckResult _checkMediaAvailability(List<String> mediaFileNames) {
		MediaCheckResult result = new MediaCheckResult();

		List<Entry> entries = JournalService.INSTANCE.getEntries();
		for (Entry entry : entries) {
			for (Event event : entry.events) {
				for (MediaItem mediaItem : event.media) {
					String filename = null;
					if (mediaItem instanceof EventPicture) {
						filename = ((EventPicture) mediaItem).filename;
					}
					if (mediaItem instanceof EventVideo) {
						filename = ((EventVideo) mediaItem).filename;
					}

					if (filename != null) {
						boolean existed = mediaFileNames.remove(filename);
						if (!existed) {
							result.itemsNotInFilesystem.add(filename);
						}
					}
				}
			}
		}

		// remaining files
		result.itemsNotInJournal.addAll(mediaFileNames);
		return result;
	}

	public static class MediaCheckResult {
		public List<String> itemsNotInFilesystem = new ArrayList<String>();
		public List<String> itemsNotInJournal = new ArrayList<String>();

		public boolean hasErrors() {
			return !itemsNotInFilesystem.isEmpty() || !itemsNotInJournal.isEmpty();
		}
	}

}
