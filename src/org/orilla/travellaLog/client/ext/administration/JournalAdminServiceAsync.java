package org.orilla.travellaLog.client.ext.administration;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Markus
 *
 */
public interface JournalAdminServiceAsync {

	/**
	 * 
	 * @see org.orilla.travellaLog.client.ext.administration.JournalAdminService#getAllMediaFiles(java.util.List)
	 */
	void getAllMediaFiles(List<String> journalNames, AsyncCallback<List<String>> callback);

}
