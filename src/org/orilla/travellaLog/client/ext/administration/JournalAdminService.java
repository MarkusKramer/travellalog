package org.orilla.travellaLog.client.ext.administration;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


/**
 * Administrative service for the blog.
 * @author Markus
 */
@RemoteServiceRelativePath("journalAdminService")
public interface JournalAdminService extends RemoteService {

	/**
	 * Returns all files located directly with the journal directories (i.e.
	 * public_journal/*) expect for 'journal.xml'.
	 */
	public List<String> getAllMediaFiles(List<String> journalNames);

}
