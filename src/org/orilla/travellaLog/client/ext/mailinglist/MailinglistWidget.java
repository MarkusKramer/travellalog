package org.orilla.travellaLog.client.ext.mailinglist;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;


/**
 * @author Markus
 * 
 */
public class MailinglistWidget extends Composite {
	interface MyUiBinder extends UiBinder<Widget, MailinglistWidget> {
		// empty
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	public MailinglistWidget() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
