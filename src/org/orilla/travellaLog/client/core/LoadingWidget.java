package org.orilla.travellaLog.client.core;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;


/**
 * @author Markus
 * 
 */
public class LoadingWidget extends Composite {

	public LoadingWidget() {
		HTMLPanel panel = new HTMLPanel("Einträge werden geladen...");
		initWidget(panel);
	}
}
