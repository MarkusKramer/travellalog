package org.orilla.travellaLog.client.core;

import java.util.List;

import org.orilla.travellaLog.client.App;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;


/**
 * @author Markus
 * 
 */
public class TitleBannerWidget extends FlowPanel {

	private Image banner;
	private List<String> imageNames;
	private int currentIndex = -1;

	private int changeIntervalMillis = 10 * 1000;

	public TitleBannerWidget() {
		addStyleName("titleBanner");
		imageNames = App.getConfig().getTitleBannerPictures();

		// build ui
		FlowPanel titleContainer = new FlowPanel();
		add(titleContainer);
		titleContainer.addStyleName("titleContainer");
		
		Image title = new Image("user/title_banner/blog_title.png");
		titleContainer.add(title);
		title.addStyleName("title");

		banner = new Image();
		add(banner);
		banner.addStyleName("banner");
		banner.setSize("760px", "125px");
		
		// init display
		show();

		Timer timer = new Timer() {
			@Override
			public void run() {
				show();
			}
		};
		timer.scheduleRepeating(changeIntervalMillis);
	}

	private void show() {
		// random
		currentIndex = (int) (Math.random() * imageNames.size());

		final String imagePath = GWT.getHostPageBaseURL() + "user/title_banner/" + imageNames.get(currentIndex);
		Log.info("Preloading title banner "+imagePath);
		
		// preload
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, imagePath);
		builder.setCallback(new RequestCallback() {
			@Override
			public void onResponseReceived(Request request, Response response) {
				// image is preloaded - change image
				banner.setUrl(imagePath);
			}

			@Override
			public void onError(Request request, Throwable exception) {
				// do nothing
			}
		});
		try {
			builder.send();
		} catch (RequestException e) {
			throw new RuntimeException("Failed to preload image "+ e.toString() + e.getMessage());
		}
	}

}
