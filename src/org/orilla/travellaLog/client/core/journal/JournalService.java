package org.orilla.travellaLog.client.core.journal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.orilla.travellaLog.client.App;
import org.orilla.travellaLog.client.core.journal.parser.EntryParser;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.util.NoCache;
import org.orilla.travellaLog.client.util.ResourceLoader;
import org.orilla.travellaLog.client.util.SimpleCallback;
import org.orilla.travellaLog.client.util.xml.XmlUtil;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.XMLParser;


/**
 * Journal Service.
 * @author Markus
 */
public enum JournalService {

	INSTANCE;

	private Map<String, List<Entry>> journalEntries = new HashMap<String, List<Entry>>();
	private int loadCount;

	/**
	 * Initially loads all available journals.
	 */
	public void init(final SimpleCallback<Void> finishedCallback) {
		loadCount = App.getConfig().getJournalNames().size();
		for (final String journalName : App.getConfig().getJournalNames()) {
			String journalUrl = GWT.getHostPageBaseURL() + journalName + "/journal.xml" + NoCache.get();
			Log.info("Loading journal '" + journalName + "' from URL " + journalUrl);

			ResourceLoader.loadXml(journalUrl, new SimpleCallback<String>() {
				public void fire(String journalXml) {
					if (journalXml != null) {
						journalEntries.put(journalName, parseJournalXml(journalXml, journalName));
						Log.info("Journal " + journalName + " loaded");
					} else {
						Log.info("Journal " + journalName + " unavailable");
					}

					// trigger callback when all journals are loaded
					loadCount--;
					if (loadCount == 0) {
						finishedCallback.fire(null);
					}
				}
			});
		}
	}

	protected List<Entry> parseJournalXml(String journalXml, String fromJournal) {
		EntryParser entryParser = new EntryParser(fromJournal);
		Document doc = XMLParser.parse(journalXml);
		List<Entry> entries = XmlUtil.parseChildren(doc.getDocumentElement(), "entry", entryParser);
		return entries;
	}

	public List<Entry> getEntries(Collection<String> journalNames, EntryFilter entryFilter, Comparator<Entry> entryOrdering) {
		Log.info("Querying for entries");
		List<Entry> entries = new ArrayList<Entry>();
		for (Entry entry : getEntries(journalNames)) {
			if (entryFilter.isValid(entry)) {
				entries.add(entry);
			}
		}
		Collections.sort(entries, entryOrdering);
		return entries;
	}

	public List<Entry> getEntries(Collection<String> journalNames) {
		List<Entry> entries = new ArrayList<Entry>();
		for (String journalName : journalNames) {
			List<Entry> jes = journalEntries.get(journalName);
			if (jes == null) {
				throw new IllegalStateException("Journal " + journalName + " not available");
			}
			entries.addAll(jes);
		}
		return entries;
	}

	/**
	 * Get the entries of all available journals.
	 */
	public List<Entry> getEntries() {
		List<Entry> entries = new ArrayList<Entry>();
		for (String journalName : getAvailableJournalNames()) {
			List<Entry> jes = journalEntries.get(journalName);
			entries.addAll(jes);
		}
		return entries;
	}

	public Set<String> getAvailableJournalNames() {
		return journalEntries.keySet();
	}

	/**
	 * Returns for every month the date of the last entry. Thus, you know for which month
	 * entries exist and when the last entry of all was.
	 */
	@SuppressWarnings("deprecation")
	public DateStatistics getDateStatistics() {
		DateStatistics stat = new DateStatistics();

		List<Entry> entries = getEntries();
		for (Entry entry : entries) {
			if (stat.latestEntryDate == null || entry.date.compareTo(stat.latestEntryDate) >= 0) {
				stat.latestEntryDate = entry.date;
			}

			// check if there is already a date if the same month
			boolean exists = false;
			for (Date month : stat.months) {
				if (entry.date.getMonth() == month.getMonth() && entry.date.getYear() == month.getYear()) {
					exists = true;
					break;
				}
			}
			if (!exists) {
				stat.months.add(entry.date);
			}

		}

		return stat;
	}

}
