package org.orilla.travellaLog.client.core.journal;

import org.orilla.travellaLog.client.domain.Entry;


/**
 * @author Markus
 * 
 */
public interface EntryFilter {

	/**
	 * Returns if the {@link Entry} complies with the filter.
	 */
	boolean isValid(Entry entry);

}
