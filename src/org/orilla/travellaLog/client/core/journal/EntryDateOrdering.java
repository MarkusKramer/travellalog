package org.orilla.travellaLog.client.core.journal;

import java.util.Comparator;

import org.orilla.travellaLog.client.domain.Entry;


/**
 * Sorts entries by date.
 * @author Markus
 */
public class EntryDateOrdering implements Comparator<Entry> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Entry e1, Entry e2) {
		int c = e1.date.compareTo(e2.date);
		return c;
	}

}
