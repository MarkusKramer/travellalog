package org.orilla.travellaLog.client.core.journal;

import org.orilla.travellaLog.client.App;
import org.orilla.travellaLog.client.domain.Event;
import org.orilla.travellaLog.client.domain.EventPicture;
import org.orilla.travellaLog.client.domain.EventVideo;
import org.orilla.travellaLog.client.domain.MediaItem;


/**
 * @author Markus
 * 
 */
public enum MediaItemLocator {

	INSTANCE;

	public String getThumbnailUrl(MediaItem mediaItem, Event event) {
		if (mediaItem instanceof EventPicture) {
			EventPicture picture = (EventPicture) mediaItem;
			String res = App.getConfig().getSmallImageMaxWidth() + "x" + App.getConfig().getSmallImageMaxHeight();
			return event.parentEntry.fromJournal + "/resized/" + res + "-" + picture.filename;
		} else if (mediaItem instanceof EventVideo) {
			return "app/mediaItem/video.jpg";
		} else {
			throw new RuntimeException("Media item type not supported: " + mediaItem.getClass());
		}
	}

	public String getLargePictureUrl(EventPicture picture, Event event) {
		String res = App.getConfig().getLargeImageMaxWidth() + "x" + App.getConfig().getLargeImageMaxHeight();
		String url = event.parentEntry.fromJournal + "/resized/" + res + "-" + picture.filename;
		return url;
	}

}
