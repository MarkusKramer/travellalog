package org.orilla.travellaLog.client.core.journal.widget;

import java.util.Arrays;

import org.orilla.travellaLog.client.TravellaLogMain;
import org.orilla.travellaLog.client.core.journal.MediaItemLocator;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.domain.Event;
import org.orilla.travellaLog.client.domain.EventLink;
import org.orilla.travellaLog.client.domain.MediaItem;
import org.orilla.travellaLog.client.ext.googleearth.GoogleEarthService;
import org.orilla.travellaLog.client.ext.googleearth.Tour;
import org.orilla.travellaLog.client.util.SimpleCallback;
import org.orilla.travellaLog.client.util.widgets.UlListPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;


/**
 * @author Markus
 * 
 */
public class JournalEntryWidget extends Composite {

	private DateTimeFormat entryDateTimeFormat = DateTimeFormat.getFormat("EEEE, d. MMMM yyyy");

	private Entry entry;

	public JournalEntryWidget(Entry entry) {
		this.entry = entry;
		buildUi();
	}

	private void buildUi() {
		Panel mainPanel = new FlowPanel();
		initWidget(mainPanel);
		mainPanel.addStyleName("entry");
		
		// facebook integration
		String entryHref = GWT.getHostPageBaseURL() + "index.html#" + TravellaLogMain.urlDateFormat.format(entry.date);
		FlowPanel facebookContainer = new FlowPanel();
		mainPanel.add(facebookContainer);
		facebookContainer.addStyleName("facebookContainer");
		integrateFacebook(facebookContainer.getElement(), entryHref);

		Panel titleWidget = new HTMLPanel(entry.title);
		mainPanel.add(titleWidget);
		titleWidget.addStyleName("title");

		Label date = new Label(entryDateTimeFormat.format(entry.date));
		mainPanel.add(date);
		date.addStyleName("date");

		// google earth
		boolean hasGeoCoordinates = false;
		for (Event event : entry.events) {
			if (event.geoCoordinate != null) {
				hasGeoCoordinates = true;
				break;
			}
		}
		if (hasGeoCoordinates) {
			Panel geoCoordinatePanel = new FlowPanel();
			mainPanel.add(geoCoordinatePanel);
			geoCoordinatePanel.addStyleName("geoCoordinate");

			Image mapImage = new Image("app/google_earth/maps_link.png");
			geoCoordinatePanel.add(mapImage);
			mapImage.addStyleName("imageButton");
			mapImage.setTitle("Auf Karte zeigen");
			mapImage.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent e) {
					Tour tour = new Tour();
					tour.name = entry.title;
					tour.entries = Arrays.asList(entry);
					GoogleEarthService.INSTANCE.showTour(tour);
				}
			});
		}

		// tour events
		Panel tourPanel = new FlowPanel();
		mainPanel.add(tourPanel);
		tourPanel.addStyleName("tour");
		for (Event event : entry.events) {
			if( event.content != null ) {
				tourPanel.add(buildEventUi(event));
			}
		}
	}

	private Widget buildEventUi(final Event event) {
		Panel mainPanel = new FlowPanel();
		mainPanel.addStyleName("event");

		// content
		HTMLPanel content = new HTMLPanel(event.content);
		mainPanel.add(content);
		content.addStyleName("content");

		// media
		mainPanel.add(buildMediaPanel(event));

		// links
		Panel linksPanel = new UlListPanel();
		mainPanel.add(linksPanel);
		linksPanel.addStyleName("links");
		for (EventLink link : event.links) {
			HTMLPanel html = new HTMLPanel("<a href='" + link.url + "' target='_blank'>" + link.title + "</a>");
			linksPanel.add(html);
		}

		return mainPanel;
	}

	private Widget buildMediaPanel(final Event event) {
		Panel mediaPanel = new FlowPanel();
		mediaPanel.addStyleName("media");

		for (final MediaItem mediaItem : event.media) {
			Panel picturePanel = new FlowPanel();
			mediaPanel.add(picturePanel);
			picturePanel.addStyleName("mediaItem");

			Image thumbnail = new Image(MediaItemLocator.INSTANCE.getThumbnailUrl(mediaItem, event));
			thumbnail.setTitle(mediaItem.getTitle());
			picturePanel.add(thumbnail);
			thumbnail.addStyleName("imageButton");
			thumbnail.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent clevent) {
					showMediaItem(event, mediaItem);
				}
			});
		}

		// add float clearer
		mediaPanel.add(new HTMLPanel("<div style=\"clear: both;\"/>"));

		return mediaPanel;
	}

	private void showMediaItem(Event event, MediaItem mediaItem) {
		MediaItemViewWidget w = new MediaItemViewWidget(event);
		w.show(mediaItem);

		final DialogBox dialogBox = w.openPopup();
		w.setFinishCallback(new SimpleCallback() {
			public void fire(Object payload) {
				dialogBox.hide();

				// scroll to next entry
				Window.scrollTo(Window.getScrollLeft(), Window.getScrollTop() + 200);
				// Element e = followingEntry.titleWidget.getElement();
				// DOM.scrollIntoView(e);
			}
		});
	}

	public Entry getEntry() {
		return entry;
	}
	
	private native void integrateFacebook(Element container, String entryHref ) /*-{
		if( $wnd.facebookIntegration != null ) {
			$wnd.facebookIntegration.integrateForElement(container,entryHref);
		}
	}-*/;

	// private void openGoogleMaps(GeoCoordinate geoCoordinate) {
	// String url = "http://maps.google.com/?q=" + geoCoordinate.ns + "," +
	// geoCoordinate.ew;
	// Window.open(url, "gmaps", "");
	// }

}
