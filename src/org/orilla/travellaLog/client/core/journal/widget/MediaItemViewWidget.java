package org.orilla.travellaLog.client.core.journal.widget;

import java.util.List;

import org.orilla.travellaLog.client.App;
import org.orilla.travellaLog.client.core.journal.MediaItemLocator;
import org.orilla.travellaLog.client.domain.Event;
import org.orilla.travellaLog.client.domain.EventPicture;
import org.orilla.travellaLog.client.domain.EventVideo;
import org.orilla.travellaLog.client.domain.MediaItem;
import org.orilla.travellaLog.client.util.SimpleCallback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;


/**
 * @author Markus
 * 
 */
public class MediaItemViewWidget extends Composite {

	private Event event;
	private List<MediaItem> mediaItems;
	private SimpleCallback finishCallback;

	private int activeMediaItem = 0;
	private Panel mediaItemPanel;

	public MediaItemViewWidget(Event event) {
		this.event = event;
		this.mediaItems = event.media;

		mediaItemPanel = new FlowPanel();
		initWidget(mediaItemPanel);
		mediaItemPanel.addStyleName("mediaItemPanel");
	}

	public void show(int i) {
		mediaItemPanel.clear();
		MediaItem mediaItem = mediaItems.get(i);

		Label mediaItemTitle = new Label(mediaItem.getTitle());
		mediaItemPanel.add(mediaItemTitle);
		mediaItemTitle.addStyleName("title");

		Panel horPanel = new HorizontalPanel();
		mediaItemPanel.add(horPanel);

		// previous
		Panel previousImagePanel = new FlowPanel();
		horPanel.add(previousImagePanel);
		previousImagePanel.addStyleName("previousImagePanel");
		if (i != 0) {
			Image previousImage = new Image("app/mediaItem/mediaItem_previous.png");
			previousImagePanel.add(previousImage);
			previousImage.addStyleName("imageButton");
			previousImage.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent e) {
					previous();
				}
			});
		}

		// media item
		Panel mediaObjectPanel;
		if (mediaItem instanceof EventPicture) {
			// picture
			EventPicture picture = (EventPicture) mediaItem;

			mediaObjectPanel = new FlowPanel();
			String url = MediaItemLocator.INSTANCE.getLargePictureUrl((EventPicture) mediaItem, event);
			Image imageWidget = new Image(url);
			mediaObjectPanel.add(imageWidget);

			Panel originalLinkPanel = new FlowPanel();
			mediaObjectPanel.add(originalLinkPanel);
			originalLinkPanel.addStyleName("originalLinkPanel");

			Anchor originalLink = new Anchor("Originalbild", event.parentEntry.fromJournal + "/" + picture.filename, "_blank");
			originalLinkPanel.add(originalLink);
		} else {
			// movie
			EventVideo video = (EventVideo) mediaItem;
			String url = event.parentEntry.fromJournal + "/" + video.filename;
			
			String absoluteUrl;
			String mediaStoreLocation = App.getConfig().getMovieStoreLocation();
			if( mediaStoreLocation != null && !GWT.getHostPageBaseURL().startsWith("file:") ) {
				absoluteUrl = mediaStoreLocation + "/" + url;
			} else {
				String docRoot = Document.get().getURL();
				docRoot = docRoot.substring(0, docRoot.lastIndexOf("/"));
				absoluteUrl = docRoot + "/" + url;
			}
			
			String filenameLc = video.filename.toLowerCase();

			if (filenameLc.endsWith(".mp4")) {
				final String finalUrl = absoluteUrl;
				mediaObjectPanel = new HTMLPanel("<p id=\"container1\">Loading media player... Flash Plugin is required</p>") {
					@Override
					protected void onAttach() {
						super.onAttach();
						int width = 640;
						int height = 480;
						startFlashPlayer(finalUrl, width, height);
					}
				};
			} else {
				mediaObjectPanel = new HTMLPanel(getWindowsMediaPlayerCode(absoluteUrl));
				horPanel.add(mediaObjectPanel);
			}
		}

		horPanel.add(mediaObjectPanel);
		mediaObjectPanel.addStyleName("mediaObjectPanel");

		// next
		Panel nextImagePanel = new FlowPanel();
		horPanel.add(nextImagePanel);
		nextImagePanel.addStyleName("nextImagePanel");
		Image nextImage;
		if (i != mediaItems.size() - 1) {
			nextImage = new Image("app/mediaItem/mediaItem_next.png");
		} else {
			nextImage = new Image("app/mediaItem/mediaItem_finish.png");
		}
		nextImagePanel.add(nextImage);
		nextImage.addStyleName("imageButton");
		nextImage.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				next();
			}
		});
	}

	public void show(MediaItem mediaItem) {
		int i = mediaItems.indexOf(mediaItem);
		if (i == -1) {
			throw new RuntimeException("MediaItem unkown");
		}
		activeMediaItem = i;
		show(activeMediaItem);
	}

	private void previous() {
		if (activeMediaItem < 1) {
			throw new IllegalStateException("There are no previous media items");
		}
		activeMediaItem--;
		show(activeMediaItem);
	}

	private void next() {
		if (activeMediaItem == mediaItems.size() - 1) {
			finishCallback.fire(null);
		} else {
			activeMediaItem++;
			show(activeMediaItem);
		}
	}

	public DialogBox openPopup() {
		DialogBox dialogBox = new DialogBox(true, false);
		dialogBox.setText(event.parentEntry.title);
		dialogBox.setWidget(this);
		dialogBox.setAnimationEnabled(true);
		dialogBox.center();
		return dialogBox;
	}

	/**
	 * @param finishCallback the finishCallback to set
	 */
	public void setFinishCallback(SimpleCallback finishCallback) {
		this.finishCallback = finishCallback;
	}

	private String getWindowsMediaPlayerCode(String url) {
		String html = "<embed type=\"application/x-mplayer2\" src=\"" + url + "\" width=\"640\" height=\"540\" />";
		return html;
	}

	private native void startFlashPlayer(String url, int width, int height) /*-{
		var flashvars = { file:url,autostart:'true' };
		var params = { allowfullscreen:'true', allowscriptaccess:'always' };
		var attributes = { id:'player1', name:'player1' };
		$wnd.swfobject.embedSWF('app/mediaItem/player.swf','container1',width,height,'9.0.115','false', flashvars, params, attributes);
	}-*/;

}
