package org.orilla.travellaLog.client.core.journal.widget;

import java.util.ArrayList;
import java.util.List;

import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.util.widgets.UlListPanel;


/**
 * @author Markus
 * 
 */
public class EntriesListViewWidget extends UlListPanel {

	private List<JournalEntryWidget> entryWidgets = new ArrayList<JournalEntryWidget>();
	
	public EntriesListViewWidget(List<Entry> journalEntries) {
		addStyleName("entriesList");

		for (int i = 0; i < journalEntries.size(); i++) {
			Entry entry = journalEntries.get(i);
			JournalEntryWidget journalEntryWidget = new JournalEntryWidget(entry);
			add(journalEntryWidget);
			entryWidgets.add(journalEntryWidget);
			
			journalEntryWidget.getElement().scrollIntoView();
		}
	}

	public List<JournalEntryWidget> getEntryWidgets() {
		return entryWidgets;
	}

}
