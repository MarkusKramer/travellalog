package org.orilla.travellaLog.client.core.journal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Stores for every month the date of the last entry. And the date of the last entry out
 * of all.
 * @author Markus
 */
public class DateStatistics {

	public List<Date> months = new ArrayList<Date>();
	public Date latestEntryDate;

}
