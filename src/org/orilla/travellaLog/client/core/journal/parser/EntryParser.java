package org.orilla.travellaLog.client.core.journal.parser;

import java.util.ArrayList;

import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.domain.Event;
import org.orilla.travellaLog.client.util.xml.ElementParser;
import org.orilla.travellaLog.client.util.xml.XmlDateParser;
import org.orilla.travellaLog.client.util.xml.XmlUtil;

import com.google.gwt.xml.client.Element;


/**
 * @author Markus
 * 
 */
public class EntryParser implements ElementParser<Entry> {

	private EventParser eventParser = new EventParser();
	private String fromJournal;

	public EntryParser(String fromJournal) {
		this.fromJournal = fromJournal;
	}

	/**
	 * @see org.orilla.travellaLog.client.util.xml.ElementParser#parse(com.google.gwt.xml.client.Element)
	 */
	@Override
	public Entry parse(Element element) {
		Entry entry = new Entry();
		entry.title = XmlUtil.getValueOfChild(element, "title");
		entry.date = XmlDateParser.parseDate(XmlUtil.getValueOfChildNullsafe(element, "date"));
		entry.dayTrip = "true".equals(element.getAttribute("dayTrip"));
		entry.fromJournal = fromJournal;

		entry.events = new ArrayList<Event>();
		Element events = XmlUtil.getChild(element, "events");
		if (events != null) {
			for (Event event : XmlUtil.parseChildren(events, "event", eventParser)) {
				event.parentEntry = entry;
				entry.events.add(event);
			}
		} else {
			Event event = eventParser.parse(element);
			event.parentEntry = entry;
			entry.events.add(event);
		}

		return entry;
	}

}
