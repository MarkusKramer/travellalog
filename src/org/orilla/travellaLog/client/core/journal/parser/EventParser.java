package org.orilla.travellaLog.client.core.journal.parser;

import java.util.ArrayList;

import org.orilla.travellaLog.client.domain.Event;
import org.orilla.travellaLog.client.domain.EventLink;
import org.orilla.travellaLog.client.domain.EventPicture;
import org.orilla.travellaLog.client.domain.EventVideo;
import org.orilla.travellaLog.client.domain.GeoCoordinate;
import org.orilla.travellaLog.client.domain.MediaItem;
import org.orilla.travellaLog.client.util.xml.ElementParser;
import org.orilla.travellaLog.client.util.xml.InvalidXmlException;
import org.orilla.travellaLog.client.util.xml.XmlUtil;

import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.NodeList;


/**
 * @author Markus
 * 
 */
public class EventParser implements ElementParser<Event> {

	/**
	 * @see org.orilla.travellaLog.client.util.xml.ElementParser#parse(com.google.gwt.xml.client.Element)
	 */
	@Override
	public Event parse(Element element) {
		Event event = new Event();
		event.title = XmlUtil.getValueOfChild(element, "title");

		// content
		boolean tourEvent = element.getParentNode().getNodeName().toLowerCase().equals("events");
		if (tourEvent) {
			event.content = XmlUtil.getValueOfChild(element, "content");
		} else {
			event.content = XmlUtil.getValueOfChildNullsafe(element, "content");
		}

		// geo
		Element geoCoEle = XmlUtil.getChild(element, "geoCoordinate");
		if (geoCoEle != null) {
			event.geoCoordinate = new GeoCoordinate(geoCoEle.getAttribute("ns"), geoCoEle.getAttribute("ew"));
		}

		// media
		event.media = new ArrayList<MediaItem>();
		Element mediaEle = XmlUtil.getChild(element, "media");
		if (mediaEle != null) {
			NodeList mediaChildren = mediaEle.getChildNodes();
			for (int i = 0; i < mediaChildren.getLength(); i++) {
				if (mediaChildren.item(i) instanceof Element) {
					Element mediaChild = (Element) mediaChildren.item(i);
					event.media.add(parseMediaItem(mediaChild));
				}
			}
		}

		// links
		event.links = new ArrayList<EventLink>();
		Element linksElement = XmlUtil.getChild(element, "links");
		if (linksElement != null) {
			NodeList linkChildren = linksElement.getChildNodes();
			for (int i = 0; i < linkChildren.getLength(); i++) {
				if (linkChildren.item(i) instanceof Element) {
					Element linkChild = (Element) linkChildren.item(i);
					event.links.add(parseLink(linkChild));
				}
			}
		}

		return event;
	}

	private MediaItem parseMediaItem(Element element) {
		if (element.getNodeName().equals("picture")) {
			EventPicture picture = new EventPicture();
			picture.title = element.getAttribute("title");
			picture.filename = XmlUtil.getValueNullsafe(element);
			return picture;
		} else if (element.getNodeName().equals("video")) {
			EventVideo video = new EventVideo();
			video.title = element.getAttribute("title");
			if (video.title == null) {
				throw new InvalidXmlException("A video must have a title", element);
			}
			video.filename = XmlUtil.getValueNullsafe(element);
			return video;
		} else {
			throw new RuntimeException("Unkown media type " + element.getNodeName());
		}
	}

	private EventLink parseLink(Element element) {
		if (element.getNodeName().equals("url")) {
			EventLink link = new EventLink();
			link.title = element.getAttribute("title");
			link.url = element.getFirstChild().getNodeValue();
			return link;
		} else {
			throw new RuntimeException("Unkown link type: " + element.getNodeName());
		}
	}

}
