package org.orilla.travellaLog.client.core.config;

import java.util.List;

import org.orilla.travellaLog.client.util.JsniUtil;

import com.google.gwt.core.client.JsArrayString;


/**
 * @author Markus
 * 
 */
public class JsConfig implements Config {

	private String evalPrefix = "appConfig.";

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getJournalNames()
	 */
	@Override
	public List<String> getJournalNames() {
		JsArrayString jsArray = evalAsArrayString(evalPrefix + "journalNames");
		List<String> journalNames = JsniUtil.toStringList(jsArray);
		return journalNames;
	}

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getLargeImageMaxWidth()
	 */
	@Override
	public int getLargeImageMaxWidth() {
		int val = evalAsInteger(evalPrefix + "mediaItem.largeImageMaxWidth");
		return val;
	}

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getLargeImageMaxHeight()
	 */
	@Override
	public int getLargeImageMaxHeight() {
		int val = evalAsInteger(evalPrefix + "mediaItem.largeImageMaxHeight");
		return val;
	}

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getSmallImageMaxWidth()
	 */
	@Override
	public int getSmallImageMaxWidth() {
		int val = evalAsInteger(evalPrefix + "mediaItem.smallImageMaxWidth");
		return val;
	}

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getSmallImageMaxHeight()
	 */
	@Override
	public int getSmallImageMaxHeight() {
		int val = evalAsInteger(evalPrefix + "mediaItem.smallImageMaxHeight");
		return val;
	}
	
	@Override
	public String getMovieStoreLocation() {
		String val = evalAsString(evalPrefix + "mediaItem.movieStoreLocation");
		return val;
	}

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getTitleBanners()
	 */
	@Override
	public List<String> getTitleBannerPictures() {
		JsArrayString jsArray = evalAsArrayString(evalPrefix + "titleBanner.pictures");
		List<String> pictures = JsniUtil.toStringList(jsArray);
		return pictures;
	}

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getGoogleMapsApiKeyDev()
	 */
	@Override
	public String getGoogleMapsApiKeyDev() {
		String val = evalAsString(evalPrefix + "googleEarth.apiKey.dev");
		return val;
	}

	/**
	 * @see org.orilla.travellaLog.client.core.config.Config#getGoogleMapsApiKeyLive()
	 */
	@Override
	public String getGoogleMapsApiKeyLive() {
		String val = evalAsString(evalPrefix + "googleEarth.apiKey.live");
		return val;
	}

	private static native int evalAsInteger(String expr) /*-{
		return $wnd.eval(expr);
	}-*/;

	private static native String evalAsString(String expr) /*-{
		return $wnd.eval(expr);
	}-*/;

	private static native JsArrayString evalAsArrayString(String expr) /*-{
		return $wnd.eval(expr);
	}-*/;

}
