package org.orilla.travellaLog.client.core.config;

import java.util.List;


/**
 * Configuration provider.
 * @author Markus
 */
public interface Config {

	public List<String> getJournalNames();

	public int getLargeImageMaxWidth();

	public int getLargeImageMaxHeight();

	public int getSmallImageMaxWidth();

	public int getSmallImageMaxHeight();

	public List<String> getTitleBannerPictures();

	public String getGoogleMapsApiKeyDev();

	public String getGoogleMapsApiKeyLive();
	
	public String getMovieStoreLocation();

}
