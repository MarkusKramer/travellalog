package org.orilla.travellaLog.client.core.entryfilter;

import java.util.Date;
import java.util.List;

import org.orilla.travellaLog.client.core.journal.EntryFilter;
import org.orilla.travellaLog.client.util.SimpleCallback;

import com.google.gwt.user.client.ui.Composite;


/**
 * @author Markus
 * 
 */
public abstract class EntriesFilterWidget extends Composite {

	public abstract EntryFilter getEntryFilter();

	public abstract void setOnChangeCallback(SimpleCallback<Object> onChangeCallback);

	public abstract List<String> getSelectedJournalNames();

	/**
	 * Make sure the given journal is not filtered out.
	 * No change callback will be triggered.
	 */
	//public abstract void addJournalToSelection(String journalName);
	
	/**
	 * Make sure the given date is not filtered out.
	 * No change callback will be triggered.
	 */
	public abstract void addDateToSelection(Date date);

}
