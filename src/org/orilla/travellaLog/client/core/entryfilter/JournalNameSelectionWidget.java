package org.orilla.travellaLog.client.core.entryfilter;

import java.util.ArrayList;
import java.util.List;

import org.orilla.travellaLog.client.App;
import org.orilla.travellaLog.client.util.SimpleCallback;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;


/**
 * @author Markus
 * 
 */
public class JournalNameSelectionWidget extends FlowPanel {

	public SimpleCallback<Object> onChangeCallback;

	private List<CheckBox> journalCheckBoxes = new ArrayList<CheckBox>();

	public JournalNameSelectionWidget() {
		addStyleName("journalSelectionPanel");

		for (String journalName : App.getConfig().getJournalNames()) {
			CheckBox checkBox = new CheckBox(journalName);
			add(checkBox);
			journalCheckBoxes.add(checkBox);

			// dirty hack
			if (journalName.equals("public_journal")) {
				checkBox.setValue(true);
			}

			checkBox.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					onChangeCallback.fire(null);
				}
			});
		}
	}

	public List<String> getSelectedJournalNames() {
		List<String> journalNames = new ArrayList<String>();
		for (CheckBox checkBox : journalCheckBoxes) {
			if (checkBox.getValue()) {
				journalNames.add(checkBox.getText());
			}
		}
		return journalNames;
	}
}
