package org.orilla.travellaLog.client.core.entryfilter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.orilla.travellaLog.client.core.journal.DateStatistics;
import org.orilla.travellaLog.client.core.journal.EntryFilter;
import org.orilla.travellaLog.client.core.journal.JournalService;
import org.orilla.travellaLog.client.domain.Entry;
import org.orilla.travellaLog.client.util.SimpleCallback;
import org.orilla.travellaLog.client.util.widgets.UlListPanel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Panel;


/**
 * A simple entry filter which only supports filtering by a month.
 * @author Markus
 */
public class MonthSelectionWidget extends EntriesFilterWidget {

	DateTimeFormat monthFormat = DateTimeFormat.getFormat("MMMM yyyy");

	public SimpleCallback<Object> onChangeCallback;

	private Panel mainPanel = new FlowPanel();
	private Panel optionsPanel;
	private List<MonthOption> monthOptions;
	private int activeMonthOption = 0;

	private JournalNameSelectionWidget journalNameSelectionWidget = new JournalNameSelectionWidget();

	public MonthSelectionWidget(DateStatistics dateStatistics) {
		initWidget(mainPanel);
		addStyleName("monthSelectionPanel");

		buildMonthOptions(dateStatistics);

		optionsPanel = new UlListPanel();
		mainPanel.add(optionsPanel);
		optionsPanel.addStyleName("optionsPanel optionsList");
		buildOptionLinks();

		// private journal option
		if (JournalService.INSTANCE.getAvailableJournalNames().size() > 1) {
			mainPanel.add(journalNameSelectionWidget);
		}
	}

	private void buildOptionLinks() {
		optionsPanel.clear();
		
		int i = 0;
		for (MonthOption monthOption : monthOptions) {
			Panel optionPanel = new FlowPanel();
			optionsPanel.add(optionPanel);
			optionPanel.addStyleName("option");
			if (i == activeMonthOption) {
				optionPanel.addStyleName("active");
			}

			Anchor link = new Anchor(monthOption.title);
			optionPanel.add(link);

			final int final_i = i;
			link.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					activeMonthOption = final_i;

					// refresh
					buildOptionLinks();

					onChangeCallback.fire(null);
				}
			});

			i++;
		}
	}

	private void buildMonthOptions(DateStatistics dateStatistics) {
		monthOptions = new ArrayList<MonthOption>();
		Date lastDate = dateStatistics.latestEntryDate;
		if (lastDate == null) {
			lastDate = new Date();
		}

		// last 30 days option
		{
			MonthOption monthOption = new MonthOption("Letzte 30 Tage");
			monthOption.end = lastDate;
			long millisPerDay = 1000 * 60 * 60 * 24;
			long millis30days = millisPerDay * 30;
			monthOption.start = new Date(monthOption.end.getTime() - millis30days);
			monthOptions.add(monthOption);
		}

		// month options
		for (Date month : dateStatistics.months) {
			MonthOption monthOption = new MonthOption(monthFormat.format(month));
			setMonthInterval(month, monthOption);
			monthOptions.add(monthOption);
		}

		// all
		MonthOption monthOption = new MonthOption("Alle Einträge");
		monthOption.end = lastDate;
		monthOption.start = new Date(1);
		monthOptions.add(monthOption);
	}

	public EntryFilter getEntryFilter() {
		return new EntryFilter() {
			public boolean isValid(Entry entry) {
				// date
				MonthOption mo = monthOptions.get(activeMonthOption);
				if (entry.date.compareTo(mo.start) < 0 || entry.date.compareTo(mo.end) > 0) {
					return false;
				}
				return true;
			}
		};
	}

	/**
	 * @see org.orilla.travellaLog.client.core.entryfilter.EntriesFilterWidget#getSelectedJournalNames()
	 */
	@Override
	public List<String> getSelectedJournalNames() {
		return journalNameSelectionWidget.getSelectedJournalNames();
	}

	/**
	 * @param onChangeCallback the onChangeCallback to set
	 */
	public void setOnChangeCallback(SimpleCallback<Object> onChangeCallback) {
		this.onChangeCallback = onChangeCallback;
		journalNameSelectionWidget.onChangeCallback = onChangeCallback;
	}

	/**
	 * Sets he a {@link DateInterval} from the 1st to the last day of the given month.
	 */
	@SuppressWarnings("deprecation")
	public void setMonthInterval(Date date, MonthOption monthOption) {
		// start date
		Date start = (Date) date.clone();
		start.setDate(1);
		start.setHours(0);
		start.setMinutes(0);
		start.setSeconds(0);
		// end date
		Date end = (Date) start.clone();
		if (end.getMonth() + 1 <= 11) {
			end.setMonth(end.getMonth() + 1);
		} else {
			end.setMonth(0);
			end.setYear(end.getYear() + 1);
		}
		long millisPerDay = 1000 * 10;
		end.setTime(end.getTime() - millisPerDay);
		monthOption.start = start;
		monthOption.end = end;
	}
	
	@Override
	public void addDateToSelection(Date date) {
		int i = 0;
		for (MonthOption monthOption : monthOptions) {
			if( monthOption.start.before(date) && monthOption.end.after(date) ) {
				activeMonthOption = i;
				buildOptionLinks();
				break;
			}

			i++;
		}
	}

	public static class MonthOption {
		public String title;
		public Date start;
		public Date end;

		public MonthOption(String title) {
			this.title = title;
		}
	}
}
