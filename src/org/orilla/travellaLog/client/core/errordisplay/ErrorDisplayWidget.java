package org.orilla.travellaLog.client.core.errordisplay;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;


/**
 * @author Markus
 * 
 */
public class ErrorDisplayWidget extends Composite {
	interface MyUiBinder extends UiBinder<Widget, ErrorDisplayWidget> {
		// empty
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	@UiField
	SpanElement message;

	public ErrorDisplayWidget(Throwable throwable) {
		initWidget(uiBinder.createAndBindUi(this));
		
		String error = throwableToString(throwable);
		error = error.replace("<", "&lt;");
		error = error.replace(">", "&gt;");
		error = error.replace("\n", "<br/>\n");
		message.setInnerHTML(error);
	}

	private String throwableToString(Throwable t) {
		StringBuffer out = new StringBuffer();
		out.append(t.getClass().getName());
		out.append(": ");
		out.append(t.getMessage());
		out.append('\n');
		
	    Object[] stackTrace = t.getStackTrace();
	    if (stackTrace != null) {
	        for (Object line : stackTrace) {
	            out.append(line);
	            out.append('\n');
	        }
	    } else {
	        out.append("[stack unavailable]");
	    }
	    
	    return out.toString();
	}
	
}
