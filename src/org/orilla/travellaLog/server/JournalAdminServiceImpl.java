package org.orilla.travellaLog.server;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.orilla.travellaLog.client.ext.administration.JournalAdminService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/**
 * @author Markus
 * 
 */
public class JournalAdminServiceImpl extends RemoteServiceServlet implements JournalAdminService {

	/**
	 * @see org.orilla.travellaLog.client.ext.administration.JournalAdminService#getAllMediaFiles(java.util.List)
	 */
	@Override
	public List<String> getAllMediaFiles(List<String> journalNames) {
		List<String> mediaFileNames = new ArrayList<String>();
		for (String journalName : journalNames) {
			File mediaDir = new File(journalName);
			for (File file : mediaDir.listFiles()) {
				if (file.isDirectory() || file.getName().equals("journal.xml")) {
					continue;
				}
				mediaFileNames.add(file.getName());
			}
		}
		return mediaFileNames;
	}

}
