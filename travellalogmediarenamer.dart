import 'dart:io';

main() {
  var options = new Options();
  String workDir = new Directory("/markus/home/Australien/blog/web/public_journal").path; //new File(options.script).directory;
  var inputXmlPath = workDir + "/journal.xml";
  var outputXmlPath = workDir + "/journal_new.xml";
  
  var outputXml = new File(outputXmlPath).openWrite();
  
  String currentDate = null;
  int currentMediaIndex = null;
  
  new File(inputXmlPath).readAsLinesSync().forEach((inputLine) {
    String outputLine = inputLine;
    
    var dateRegex = new RegExp(r'<date>(.*)</date>');
    dateRegex.allMatches(inputLine).forEach((match) {
      currentDate = match.group(1);
      currentMediaIndex = 1;
      print("curent date: "+currentDate);
    });
    
    var renameMedia = (String oldFilename) {
      String ending = oldFilename.substring(oldFilename.lastIndexOf('.'));
      int i = currentMediaIndex++;
      String newFilename = currentDate + "_" + i.toString() + ending.toLowerCase();
      print("Renaming media from "+oldFilename+" to "+newFilename);
      new File(workDir + "/" + oldFilename).renameSync(workDir + "/" + newFilename);
      return newFilename;
    };
    
    var processMedia = (Match match) {
      var start = match.group(1); 
      var oldFilename = match.group(3);
      var end = match.group(4);
      String newFilename = renameMedia(oldFilename);
      outputLine = start + newFilename + end;
    };
    
    new RegExp(r'(.*<(picture|video).*>)(.*)(</(picture|video)>)')
      .allMatches(inputLine)
      .forEach(processMedia);
    
    outputXml.writeln(outputLine);
  });
  
  outputXml.close();
}
