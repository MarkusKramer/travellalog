#!/bin/sh

VERSION=0.9.0
RELEASE_DIR=./travellalog-$VERSION
RELEASE_ARCHIVE=travellalog-$VERSION.zip
SOURCE_PATH=./
#http://travellalog.googlecode.com/svn/tags/$VERSION

rm -rf $RELEASE_DIR $RELEASE_ARCHIVE
mkdir -v $RELEASE_DIR

svn export $SOURCE_PATH/war $RELEASE_DIR/web
svn export $SOURCE_PATH/tools $RELEASE_DIR/tools
cp -r $SOURCE_PATH/war/travellalog $RELEASE_DIR/web
rm -r $RELEASE_DIR/web/WEB-INF

zip -r $RELEASE_ARCHIVE $RELEASE_DIR/*
rm -rf $RELEASE_DIR

