
appConfig = {
		journalNames: ["public_journal"],
		
		mediaItem: {
			largeImageMaxWidth: 750,
			largeImageMaxHeight: 530,
			smallImageMaxWidth: 100,
			smallImageMaxHeight: 70,
			movieStoreLocation: null
		},
		
		titleBanner: {
			pictures: [
				"blue_mountains.jpg",
				"kangaroos.jpg",
				"uluru_sunset.jpg"
			]
		},
		
		googleEarth: {
			apiKey: {
				dev: "ABQIAAAALZP2xR3rDPnM6jrWqEnScBREGtQZq9OFJfHndXhPP8gxXzlLARQX2VJq5FRzUNxNrmPWhzf5FyQ4bQ",
				live: ""
			}
		}
}
