var divName;
var onEarthReady;
var ge;
var gex;
var kmlObject;


function loadGoogleEarth(_divName, googleMapsApiKey, _onEarthReady) {
	divName = _divName;
	onEarthReady = _onEarthReady;
	
	// load google loader
	var script = document.createElement("script");
	script.src = "http://www.google.com/jsapi?key=" + googleMapsApiKey + "&callback=loadGoogleEarth_continued";
	script.type = "text/javascript";
	document.getElementsByTagName("head")[0].appendChild(script);
}

function loadGoogleEarth_continued() {
	var initCB = function(instance) {
		ge = instance;
		gex = new GEarthExtensions(ge);
		ge.getWindow().setVisibility(true);

		// add a navigation control and layers
		ge.getNavigationControl().setVisibility(ge.VISIBILITY_AUTO);
		ge.getLayerRoot().enableLayerById(ge.LAYER_BORDERS, true);
		ge.getLayerRoot().enableLayerById(ge.LAYER_ROADS, true);
		ge.getOptions().setStatusBarVisibility(true);
		ge.getOptions().setScaleLegendVisibility(true);

		onEarthReady();
	};

	var failureCB = function(errorCode) {
	};

	var loadEarth = function() {
		google.earth.createInstance(divName, initCB, failureCB);
	};

	google.load("earth", "1", {"callback" : loadEarth});
}

function showKml(kmlXml) {
	kmlObject = ge.parseKml(kmlXml);
	if (kmlObject) {
		ge.getFeatures().appendChild(kmlObject);
		setTimeout('gex.util.flyToObject(kmlObject, { aspectRatio: 1 });', 0);
	} else {
		alert("Bad KML");
	}
}

/*
function loadScript(url, loadedCallback) {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.onreadystatechange = function() {
		if (this.readyState == 'complete') {
			loadedCallback();
		}
	}
	script.onload = loadedCallback;
	script.src = url;

	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(script, s);
}
*/
