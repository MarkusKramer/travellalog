<?php

header('Access-Control-Allow-Origin: *');

if( $_SERVER['REQUEST_METHOD'] == "POST" ) {
  session_start();
  $_SESSION["data"] = file_get_contents('php://input');  
  echo session_id();
} else {
  session_id($_GET["sessionId"]);
  session_start();
  header('Content-type: application/vnd.google-earth.kml+xml');
  header('Content-Disposition: attachment; filename=tour.kml');
  echo $_SESSION["data"];
}

?>
