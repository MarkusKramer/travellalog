<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dt="http://xsltsl.org/date-time" version="1.0">









<!-- THIS FILE IS NOT UP TO DATE -->












	
	<xsl:output method="html" />

    <xsl:template match="/journal">
    	<xsl:for-each select="entry">
    		<xsl:sort select="date"/>

	    	<div class="entry">
		    	<div class="title">
		    		<xsl:value-of select="title" />
		    	</div>
		    	<div class="date">
		    		<xsl:value-of select="date" />
		    	</div>
		    	<div class="content">
		    		<xsl:copy-of select="content"  />
		    	</div>
	
		    	<ul class="pictures">
					<xsl:for-each select="pictures/picture">
						<li class="picture">
							<a href="{../../@fromJournal}/resized/600x600-{current()}">
								<img src="{../../@fromJournal}/resized/90x70-{current()}" title="{@title}" />
								<!--
								<xsl:choose>
									<xsl:when test="@title">
										<xsl:value-of select="@title" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="current()" />
									</xsl:otherwise>
								</xsl:choose>
								-->
							</a>
						</li>
					</xsl:for-each>
		    	</ul>
	        </div>
        
    	</xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
