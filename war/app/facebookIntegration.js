
function FacebookIntegration() {
}

FacebookIntegration.prototype = {
	fbInitQueue: new Array(),
	
	init: function() {
		var _instance = this;
//		$("body").append('<div id="fb-root"></div>');
		
		window.fbAsyncInit = function() {
			// init the FB JS SDK
			FB.init({
				appId : _instance.getAppId(),
				status : false, // check the login status upon init?
				cookie : false, // set sessions cookies to allow your server to
								// access the session?
				xfbml : false   // parse XFBML tags on this page?
			});
			
			$.each(_instance.fbInitQueue, function(){
				this();
			});
		};

		// Load the SDK's source Asynchronously
		(function(d, debug) {
			var js, id = 'facebook-jssdk', ref = d
					.getElementsByTagName('script')[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement('script');
			js.id = id;
			js.async = true;
			js.src = "//connect.facebook.net/en_US/all"
					+ (debug ? "/debug" : "") + ".js";
			ref.parentNode.insertBefore(js, ref);
		}(document, false));
	},
	
	runWithFB: function(fct) {
		if( window.FB != null ) {
			fct();
		} else {
			this.fbInitQueue.push(fct);
		}
		
	},
	
	integrateForElement: function(element, entryHref) {
		var _instance = this;
		
		$(element).append('<p class="loadingSocial">Loading social features...</p>');
		
		this.runWithFB(function(){
			_instance._integrateForElementAfterFBReady(element, entryHref);
		});
	},
	
	_integrateForElementAfterFBReady: function(element, entryHref) {
		var _instance = this;
		console.log("integrateForElement", element, entryHref);
		
		// like
		$(element).append("<div class='fb-like' data-href='"+entryHref+"' data-send='true' data-width='"+this.getContainerWidth()+"' data-show-faces='true'></div>");
		FB.XFBML.parse(element);
		
		// comments
		var commentContainer = $('<div class="commentContainer">');
		$(element).append(commentContainer);
		
		$(element).find("p.loadingSocial").remove();
		
		$.ajax({
			url: "http://graph.facebook.com/"+encodeURIComponent(entryHref),
			dataType: "json"
		}).done(function(result) {
			_instance._addComments(commentContainer, entryHref);
			/*
			if( result.comments != null && result.comments > 0 ) {
				_instance._addComments(commentContainer, entryHref);
			} else {
				var commentLink = $('<a href="#">Kommentar schreiben</a>');
				commentContainer.append(commentLink);
				commentLink.click(function() {
					commentLink.fadeOut(1000, function() {
						$(this).remove();
					});
					_instance._addComments(commentContainer, entryHref);
				});
			}
			*/
			
		}).fail(function( xmlHttpRequest, statusText, errorThrown ) {
			$(element).append('<p class="errorMessage">'+statusText+'</p>');
			console.error(xmlHttpRequest, statusText, errorThrown);
		});
	},
	
	_addComments: function(element, entryHref) {
		var commentsDiv = "<div class='fb-comments' data-href='"+entryHref+"' data-width='"+this.getContainerWidth()+"' data-num-posts='5'></div>";
		element.append(commentsDiv);
		var domNode = element.get()[0];
		FB.XFBML.parse(domNode);
	},
	
	getAppId: function() {
		return "173882829402841";
	},
	
	getContainerWidth: function() {
		return "380px";
	}
};

var facebookIntegration = new FacebookIntegration();
facebookIntegration.init();
