<html><body>

<?php
$xmlFile = "comments.xml";
$xmlFragment = $_REQUEST["xml"];



//error handler function
function customError($errno, $errstr, $error_file, $error_line) {
  die("<b>Error:</b> [$errno] $errstr (line $error_file, line $error_line");
}
set_error_handler("customError");


_addFragmentToXml($xmlFile, $xmlFragment);
echo("SUCCESS");


/**
 * Adds a fragment to the end of an XML file.
 * Creates it if it doesn't exist.
 */
function _addFragmentToXml($xmlFile, $xmlFragment) {
	$xmlContent = _readFile($xmlFile);

	// split content
	$rootClosePos = strripos($xmlContent,"</");
	$part1 = substr($xmlContent, 0, $rootClosePos);
	$remainder = strlen($xmlContent)-$rootClosePos;
	$part2 = substr($xmlContent, $rootClosePos, $remainder);
	
	// insert and write
	$newContent = $part1.$xmlFragment."\n".$part2;
	_writeFile($xmlFile,$newContent);
}


function _readFile($filename) {
	$content = "";
	$file_handle = fopen($filename, "r");
	if( $file_handle == FALSE ) {
		die("Couldnt open file $filename");
	}

	while (!feof($file_handle)) {
	   $line = fgets($file_handle);
	   $content = $content.$line;
	}
	fclose($file_handle);
	return $content;
}


function _writeFile($filename,$content) {
	$file = fopen ($filename, "w");
	fwrite($file, $content);
	fclose ($file);
}

?>

</body></html>
